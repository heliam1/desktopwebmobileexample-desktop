var fs = require('fs');
var config = require('../configs/devConfig.json');
try {
  
  fs.writeFileSync('app/firebaseConfigGenerated.json', JSON.stringify(config), 'utf8');
} catch (err) {
  console.log(err);
}

var args = process.argv.slice(2);
if (args[0] === "--prod") {
  var testE2eConfigProd = require('../config/prodConfig.json');
  testE2eConfigProd.userAgent = 'win';
  try {
    fs.writeFileSync('e2e-test/firebaseConfigGenerated.json', JSON.stringify(testE2eConfigProd), 'utf8');
    console.log("Ready to test prod.");
  } catch (err) {
    console.log(err);
  }
} else if (args[0] === "--test") {
  var testE2eConfigTest = require('../config/testConfig.json');
  testE2eConfigTest.userAgent = 'win';
  try {
    fs.writeFileSync('e2e-test/firebaseConfigGenerated.json', JSON.stringify(testE2eConfigTest), 'utf8');
    console.log("Ready to test the test environment.");
  } catch (err) {
    console.log(err);
  }
} else if (args[0] === "--dev") {
  var testE2eConfigDev = require('../config/devConfig.json');
  testE2eConfigDev.userAgent = 'win';
  try {
    fs.writeFileSync('e2e-test/firebaseConfigGenerated.json', JSON.stringify(testE2eConfigDev), 'utf8');
    console.log("Ready to test dev environment.");
  } catch (err) {
    console.log(err);
  }
} else /*if (args[0] === "--local")*/ {
  var testE2eConfigLocal = require('../config/localConfig.json');
  testE2eConfigLocal.userAgent = 'win';
  try {
    fs.writeFileSync('e2e-test/firebaseConfigGenerated.json', JSON.stringify(testE2eConfigLocal), 'utf8');
    console.log("Ready to test local dev environment - localhost:3000.");
  } catch (err) {
    console.log(err);
  }
}
