var admin = require("firebase-admin");
var config = require("../app/firebaseConfigGenerated.json")
const {Storage} = require('@google-cloud/storage');

(async () => {
  let serviceAccount;

  if (config.projectId.includes('dev')) {
    serviceAccount = require('./desktop-web-mobile-dev-firebase-adminsdk-1765k-3fcd3a3eb1.json');
  } else if (config.projectId.includes('test')) {
    serviceAccount = {
      type: process.env.TYPE_TEST_SA,
      project_id: process.env.PROJECT_ID_TEST_SA,
      private_key_id: process.env.PRIVATE_KEY_ID_TEST_SA,
      private_key: process.env.PRIVATE_KEY_TEST_SA.replace(/\\n/g, '\n'),
      client_email: process.env.CLIENT_EMAIL_TEST_SA,
      client_id: process.env.CLIENT_ID_TEST_SA,
      auth_uri: process.env.AUTH_URI_TEST_SA,
      token_uri: process.env.TOKEN_URI_TEST_SA,
      auth_provider_x509_cert_url: process.env.AUTH_PROVIDER_X509_CERT_URL_TEST_SA,
      client_x509_cert_url: process.env.CLIENT_X509_CERT_URL_TEST_SA
    }
  } else if (config.projectId === 'desktop-web-mobile') {
    serviceAccount = {
      type: process.env.TYPE_PROD_SA,
      project_id: process.env.PROJECT_ID_PROD_SA,
      private_key_id: process.env.PRIVATE_KEY_ID_PROD_SA,
      private_key: process.env.PRIVATE_KEY_PROD_SA.replace(/\\n/g, '\n'),
      client_email: process.env.CLIENT_EMAIL_PROD_SA,
      client_id: process.env.CLIENT_ID_PROD_SA,
      auth_uri: process.env.AUTH_URI_PROD_SA,
      token_uri: process.env.TOKEN_URI_PROD_SA,
      auth_provider_x509_cert_url: process.env.AUTH_PROVIDER_X509_CERT_URL_PROD_SA,
      client_x509_cert_url: process.env.CLIENT_X509_CERT_URL_PROD_SA
    }
  } else {
    throw new Error('This script does not know which environment it is running for.');
  }

  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    storageBucket: config.storageBucket,
  });
  
  // 'bucket' is an object defined in the @google-cloud/storage library.
  // See https://googlecloudplatform.github.io/google-cloud-node/#/docs/storage/latest/storage/bucket
  // for more details.
  var bucket = admin.storage().bucket();
  
  const promises = [];

  try {
    promises.push(
      bucket.upload(
        'release/desktopwebmobileexample-desktop Setup 1.0.0.exe',
        {
          gzip: true,
          destination: 'desktopwebmobileexample-desktop Setup 1.0.0.exe',
          metadata: {
            // cacheControl: 'public, max-age=3600',
          }
        }
      )
    );
    promises.push(
      bucket.upload(
        'release/latest.yml',
        {
          gzip: true,
          destination: 'latest.yml',
          metadata: {
            // cacheControl: 'public, max-age=3600',
          }
        }
      )
    );
    await Promise.all(promises);
  } catch (err) {
    console.log(err);
  }
})();


