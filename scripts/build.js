var fs = require('fs');
var args = process.argv.slice(2);
if (args[0] === "--prod") {
  var config = require('../configs/prodConfig.json');
  config.userAgent = 'win';
  try {
    fs.writeFileSync('app/firebaseConfigGenerated.json', JSON.stringify(config), 'utf8');
  } catch (err) {
    console.log(err);
  }
} else /*if (args[0] === "--dev")*/ {
  var config = require('../configs/devConfig.json');
  config.userAgent = 'win';
  try {
    fs.writeFileSync('app/firebaseConfigGenerated.json', JSON.stringify(config), 'utf8');
  } catch (err) {
    console.log(err);
  }
}
