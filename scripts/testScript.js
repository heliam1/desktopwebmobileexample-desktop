const fs = require('fs');

console.log("Make sure the dev/test/prod environment you are testing is live. E.g. for dev, run yarn dev before this test.");

var args = process.argv.slice(2);
if (args[0] === "--prod") {
  var testE2eConfigProd = require('../config/prodConfig.json');
  try {
    fs.writeFileSync('e2e-test/firebaseConfigGenerated.json', JSON.stringify(testE2eConfigProd), 'utf8');
    console.log("Ready to test prod.");
  } catch (err) {
    console.log(err);
  }
} else if (args[0] === "--test") {
  var testE2eConfigTest = require('../config/testConfig.json');
  try {
    fs.writeFileSync('e2e-test/firebaseConfigGenerated.json', JSON.stringify(testE2eConfigTest), 'utf8');
    console.log("Ready to test the test environment.");
  } catch (err) {
    console.log(err);
  }
} else if (args[0] === "--dev") {
  var testE2eConfigDev = require('../config/devConfig.json');
  try {
    fs.writeFileSync('e2e-test/firebaseConfigGenerated.json', JSON.stringify(testE2eConfigDev), 'utf8');
    console.log("Ready to test dev environment.");
  } catch (err) {
    console.log(err);
  }
} else /*if (args[0] === "--local")*/ {
  var testE2eConfigLocal = require('../config/localConfig.json');
  try {
    fs.writeFileSync('e2e-test/firebaseConfigGenerated.json', JSON.stringify(testE2eConfigLocal), 'utf8');
    console.log("Ready to test local dev environment - localhost:3000.");
  } catch (err) {
    console.log(err);
  }
}
