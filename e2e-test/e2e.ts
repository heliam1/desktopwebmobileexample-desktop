import puppeteer from 'puppeteer';
import config from './firebaseConfigGenerated.json';
import testCredentials from './testCredentials.json';
//import * as admin from 'firebase-admin';
//import { RemoteDbImpl } from '../remoteDb/RemoteDbImpl';
//import { AuthImpl } from '../auth/AuthImpl';
import axios from 'axios';
import { getAccessToken } from './accessToken';

let userEmail = '';

export function getUserEmail() {
  return userEmail;
}

/*
Example
  test('should be titled "Google"', async () => {
    const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
    const page = await browser.newPage();
    await page.goto('https://google.com');
    await expect(page.title()).resolves.toMatch('Google');
    await browser.close();
  });

  test('1 + 1', async () => {
    expect(1 + 1).toEqual(2);
  });
*/

// TODO: incognito context?
// Lazily creates Browser and page
export async function createBrowserAndPageAndNavigateToDomain(): Promise<any> {

  let protocol = 'https://';
  let domain = config.domain;
  let headless = true;
  
  const projectId = process.env.GCP_PROJECT || process.env.GCLOUD_PROJECT;

  if (!projectId && config.domain === 'localhost:3000') {
    protocol = 'http://';
    headless = false;
    domain = "localhost:3000";
    console.log('headless: false');
  }

  const args = ['--no-sandbox', '--disable-setuid-sandbox'];
  if (headless) args.push('--single-process');

  const browser = await puppeteer.launch({
    args,
    headless: headless,
    pipe: true,
  });

  console.log('Creating new page');
  const page = await browser.newPage();

  console.log(`Navigating to ${protocol + domain}`);
  // await page.waitForNavigation();
  await page.goto(protocol + domain + "/#/", {  
    waitUntil: 'domcontentloaded',
  });

  // For whatever reason when testing on localhost we must wait longer than domcontentloaded
  await page.waitFor(1000);

  return { browser, page };
}

export async function navigateToUnauthenticatedPage(page: puppeteer.Page) {
  let protocol = 'https://';
  let domain = config.domain;
  
  const projectId = process.env.GCP_PROJECT || process.env.GCLOUD_PROJECT;

  if (!projectId) {
    protocol = 'http://';
    domain = "localhost:3000";
  }

  await page.goto(protocol + domain + "/#/" + "calendar/day");
  await page.waitFor(1000);
  if (page.url() !== protocol + domain + "/#/") {
    throw new Error("Unauthenticated user was not redirected from authenticated page.");
  } 
}


export async function signUp(page: puppeteer.Page) {
  console.log('Attempting sign up.');

  await page.waitFor(7500);

  // const inputs = await page.$$('input');
  // emailInput = inputs[0];
  const emailInput = await page.$('[data-testid="home_email"]');
  await emailInput!.click();
  await emailInput!.type(testCredentials.email);

  const pwInput = await page.$('[data-testid="home_password"]');
  await pwInput!.click();
  await pwInput!.type(testCredentials.pw);

  let buttons = await page.$$('button');
  let signUpButton = buttons[1];
  await signUpButton.click();

  await page.waitForResponse(`https://us-central1-${config.projectId}.cloudfunctions.net/signUpUserWithEmailAndPassword`);
  await page.waitFor(1000);
  // or perhaps wait for response
  // await page.waitForSelector('[data-testid="home_success"]');
}

export async function logIn(page: puppeteer.Page) {
  console.log('Attempting log in.');

  // log in
  // Need to get the email input again because of "Error: Node is detached from document".
  const emailInput = await page.$('[data-testid="home_email"]');
  await emailInput!.click().catch((err: Error) => console.log("emailInput.click(): " + err));
  await emailInput!.type(testCredentials.email).catch((err: Error) => console.log("emailInput.type(): " + err));

  console.log('Email input. Inputting password.');

  const pwInput = await page.$('[data-testid="home_password"]');
  await pwInput!.click().catch((err: Error) => console.log("pwInput.click(): " + err));
  await pwInput!.type(testCredentials.pw).catch((err: Error) => console.log("pwInput.type(): " + err));

  console.log('Finding and clicking logging in button.');

  const buttons = await page.$$('button');
  const logInButton = buttons[0];
  logInButton.click().catch((err: Error) => console.log("logInButton.click(): " + err));;

  console.log('Waiting for response and navigation.');

  // Notice that this URL is requested after login has already been verified and we are now getting the user's info to client side.
  await Promise.all([
    page.waitFor(1000), // page.waitForNavigation(),
    page.waitForResponse((response: { url: () => string | string[]; }) => {
      return response.url().includes('googleapis.com/identitytoolkit/v3/relyingparty/getAccountInfo');
    }),
    // 
  ]);

  console.log('Response and navigation complete.');
}

export async function addAnEvent(page: puppeteer.Page, eventNameKey: string) {
  console.log(`Adding event${eventNameKey}`);

  await page.waitForSelector('[data-testid="calendar_day_fab"]');

  let calendarDayFab = await page.$('[data-testid="calendar_day_fab"]');

  await Promise.all([
    page.waitFor(1500), // page.waitForNavigation(),
    calendarDayFab!.click(),
  ]);

  // wait for loading to go away
  // await page.waitForSelector('[data-testid="event_loading"]', { hidden: true });
  // document.querySelector('[data-testid="event_name"]').value // this in the browser console displays the text. can use .innerText for others.
  // TODO: might be able to wait for certain text. i.e. newEvent and not loading
  // await page.waitFor(1000);
  let eventName = await page.$('[data-testid="event_name"]');

  await eventName!.click();
  await eventName!.type(`My event name${eventNameKey}`);
  let eventSave = await page.$('[data-testid="event_save"]');
  await Promise.all([
    page.waitFor(1000), // page.waitForNavigation(),
    eventSave!.click(),
  ]);
}

// use console selectors to find element
    // document.querySelectorAll('button');
    // document.querySelectorAll('input');
// we can see <p> is pretty unique
    // this a unique element, as opposed to class name
    //   - to discriminate by element, simply use the element name
    //   - to discriminate by class name we must prepend . to the class name e.g. .className123
    //   - to discriminate by id we prepend #          
    //   - to discriminate by data-testid or other attirbutes we use '[data-testid="event_save"]'
    //   - to discriminate by text we use  Array.from(await page.$$('p')).find(el => el.textContent === 'My event name 4');
    //   - we may need to perform alternative discrimination - by selecting all buttons on a page or element, and then their position in this array
    //   - as a last resort can use x path
    //   - others: https://www.w3schools.com/cssref/css_selectors.asp
    // we want to click on the <p> with the text content of "My event name4"
    // 
    // we can fast find the discriminator by using console in dev tools on the page. document.querySelector = page.$
    // document.querySelectorAll = page.$$
// For scrolling concerns    
    // https://stackoverflow.com/questions/51529332/puppeteer-scroll-down-until-you-cant-anymore/53527984
export async function clickAnEvent(page: puppeteer.Page, eventNameKey: string) {
  console.log('Waiting for Firestore');
  // const finalResponse = await page.waitForResponse(response => );
  // firestore sends 2 responses back in order to load the data. Not sure why. Perhaps something to do with web socket / protocol upgrade.
  //await page.waitForResponse(response => {
  //  return response.url().includes('firestore.googleapis.com');
  //}); //.then(() => {
    //return page.waitForResponse(response => {
    //  return response.url().includes('firestore.googleapis.com');
    //});
  //});
  // Waiting for 2 responses isn't working out all the time. Perhaps it's due to the content needing to be loaded.
  // waiting for 2 responses might only be required fro the initial connection + data returned.
  // Be aware that Firestore listener will update in the background if we have other ui open and a change occurs
  await page.waitFor(1000);
  await page.waitForSelector('p');
  
  console.log('Got Firestore');

  // I see a few options:
  // find by text using SO
  // find all p, go through them all, find a match.
  var listItems = await page.$$('p');
  console.log(`list items length: ${listItems.length}`);
  let positionOfMatch = -1;
  for (let i = 0; i < listItems.length; i++) {
    const element = listItems[i];
    
    // OR const text = await (await element.getProperty('textContent')).jsonValue();
    const text = await page.evaluate((element: any) => element.textContent, element);
    // Can probs combine $$ and .evaluate into $$eval, not sure though.

    console.log(text);

    if (text === eventNameKey) {
      positionOfMatch = i;
      console.log(`Position of match: ${i}`);
      break;
    }
  }
    
  listItems = await page.$$('p');
  await Promise.all([
    page.waitFor(1000), // page.waitForNavigation(),
    listItems[positionOfMatch].click(),
  ]);
}

export async function modifyAnEvent(page: puppeteer.Page, eventNameKey: string) {
  console.log(`Modifying event ${eventNameKey}.`);
  // user modifies an event
  await page.waitFor(1000);
  let eventName = await page.$('[data-testid="event_name"]');
  const inputValue = await page.$eval('[data-testid="event_name"]', (el: any) => el.value);
  await eventName!.click();
  console.log(`input length: ${inputValue.length}`);
  for (let i = 0; i < inputValue.length; i++) {
    await page.keyboard.press('ArrowRight');
  }
  for (let i = 0; i < inputValue.length; i++) {
    await page.keyboard.press('Backspace');
  }
  await eventName!.type(eventNameKey);

  let eventSave = await page.$('[data-testid="event_save"]');
  await Promise.all([
    page.waitFor(1000), // page.waitForNavigation(),
    eventSave!.click(),
  ]);

  console.log("Modified event successfully");
  await page.waitFor(1000);
}

export async function deleteAnEvent(page: puppeteer.Page) {
  console.log("Deleting event.");

  // user deletes an event
  await page.waitFor(1000);
  let eventDelete = await page.$('[data-testid="event_delete"]');
  await Promise.all([
    page.waitForNavigation(),
    eventDelete!.click(),
  ]);
  
  console.log("Deleted event successfully");
  await page.waitFor(1000);
}

export async function navigateToSettings(page: puppeteer.Page) {
  console.log('Navigating to settings page.')
  // user navigates to settings
  await page.waitFor(1000);
  let navProfile = await page.$('[data-testid="nav_profile"]');
  await Promise.all([
    page.waitFor(1000), // page.waitForNavigation(),
    navProfile!.click(),
  ]);
  await page.waitFor(1000);
}

export async function signOut(page: puppeteer.Page) {
  console.log('Signing out.');
  let signOut = await page.$('[data-testid="settings_sign_out"]');
  await Promise.all([
    page.waitFor(1000), // page.waitForNavigation(),
    signOut!.click(),
  ]);
}

export async function deleteAccount(page: puppeteer.Page) {
  console.log('Deleting account.');
  let deleteAccount = await page.$('[data-testid="settings_delete_account"]');
  await Promise.all([
    page.waitFor(1000), // page.waitForNavigation(),
    deleteAccount!.click(),
  ]);
  userEmail = '';
}

// This cleans up the browser
export async function close(browser: puppeteer.Browser) {
  try {
    await browser.close();
    return;
  } catch (err) {
    console.log(`Browser may have already been closed: ${err}`);
    return;
  }
}

// This cleans up remoteDb and Auth, not Browser
export async function cleanUp(userEmail: string) {
  console.log('Cleaning up.');

  if (userEmail !== '') {
    try {
      const projectId = process.env.GCP_PROJECT || process.env.GCLOUD_PROJECT;
  
      // If running e2e test locally
      if (!projectId) {
        console.log('Authenticating with a service account since not on Google environment.');
    
        // We use API key when running e2e test locally. Would pref to use OAuth via service account though.

        // var serviceAccount = await require('./desktop-web-mobile-dev-firebase-adminsdk-1765k-3fcd3a3eb1.json');
        // We need to authenticate with a service account if running the test locally.
        // admin.initializeApp({
        //  projectId: config.projectId,
        //  credential: admin.credential.cert(serviceAccount),
        //  databaseURL: "https://desktop-web-mobile-dev.firebaseio.com"
        //});
  
        const accessToken = await getAccessToken();
  
        console.log('Sending clean up request')
        console.log('User email: ' + userEmail);

        await axios({
          method: 'post',
          headers: {
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': 'Bearer ' + accessToken
          },
          url: `https://us-central1-${config.projectId}.cloudfunctions.net/testCleanUpE2e`,
          
          data: {
            userEmail,
          },
        });
        
      // if running e2e test in cloud function
      // we don't include service account in git repo, we use application default credentials if running from cloud function
      // app is already initialized
      } else {
        throw new Error("Currently this code does not run on a Google Environment.");
        //const auth = new AuthImpl();
        //const remoteDb = new RemoteDbImpl();
      
        //console.log(`Finding user id for user email ${userEmail}.`)
        //const userId = await remoteDb.findUserIdForEmail(userEmail);
        //console.log(`User id: ${userId}.`)
        //await remoteDb.deleteUser(userId);
        //console.log('Deleted user from remote database.')
        //await auth.deleteUser(userId);
        //console.log('Deleted user from authentication.')
      }
    } catch (err) {
      console.log('Couldn\'t clean up environment due to:');
      console.log(err);
      throw new Error("Error cleaning up. The environment may need to be manually cleaned.");
    }
  }
  console.log('Clean up succeeded.');
}

export async function e2e(_browser: puppeteer.Browser, page: puppeteer.Page) {
  console.log('Starting test.');

  // TODO: this should probably be configurable, and injected into signUp.
  userEmail = '';
  
  // TODO: test redirected if not logged in
  await signUp(page);
  userEmail = testCredentials.email;
  await logIn(page);
  for (let i = 0; i < 10; i++) {
    await addAnEvent(page, i.toString());
  }
  await clickAnEvent(page, "My event name9");
  await modifyAnEvent(page, "My modified event name");
  await clickAnEvent(page, "My event name7");
  await deleteAnEvent(page);
  await navigateToSettings(page);
  await navigateToSettings(page); // do it twice because chromium renderer bad
  await signOut(page);

  page.waitFor(1000);

  await logIn(page);
  await navigateToSettings(page);
  await deleteAccount(page);

  console.log('Should be complete');
}
