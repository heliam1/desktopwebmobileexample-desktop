import { e2e, close, cleanUp, getUserEmail } from "./e2e";

const electron = require("electron");
const kill = require("tree-kill");
const puppeteer = require("puppeteer");
const { spawn } = require("child_process");

const port = 9200; // Debugging port
const timeout = 30000; // Timeout in miliseconds
let browser: any;
let page: any;
let pid: any;

let errorCount = 0;

jest.setTimeout(timeout);

beforeAll(async () => {
  try {
    await cleanUp(getUserEmail());
  } catch (err) {
    console.log(err);
  }
});

afterAll(async () => {
  try {
    await cleanUp(getUserEmail());
  } catch (err) {
    console.log(err);
  }

  try {
    await page.close();
  } catch (error) {
    kill(pid);
  }
});

async function createElectronApp() {
  const startTime = Date.now();

  // Start Electron with custom debugging port
  pid = spawn(electron, ["./app/main.prod.js", `--remote-debugging-port=${port}`], {
    shell: true
  }).pid;

  const headless = true;
  const args: Array<any> = [];

  // Wait for Puppeteer to connect
  while (!browser) {
    try {
      browser = await puppeteer.connect({
        args,
        headless: headless,
        pipe: true,
        browserURL: `http://localhost:${port}`,
        defaultViewport: { width: 1000, height: 600 }
      });
      [page] = await browser.pages();
    } catch (error) {
      errorCount++;
      if (Date.now() > startTime + timeout) {
        console.log(`Error count: ${errorCount}`)
        throw error;
      }
    }
  }
}

describe("e2e", () => {
  test('e2e', async () => {
    
    await createElectronApp();

    try {
      await e2e(browser, page);
    } catch (err) {
      throw (err);
    } finally {
      await close(browser);
    }
  }, 120 * 1000);
});
