import { Epic } from 'redux-observable';
import { filter, mergeMap,catchError, mapTo, tap, map } from 'rxjs/operators';
import { isOfType } from 'typesafe-actions';
import { from, of } from 'rxjs';
// eslint-disable-next-line import/no-cycle
import { RootState, Dependencies } from '../store';
import { HomeActions, HomeActionsTypes, error, logInSuccess, signUpSuccess, anonLogInAndDownloadUrlSuccess } from './homeActions';

export const homeAnonLogInAndDownloadUrlEpic: Epic<
  HomeActions,
  HomeActions,
  RootState,
  any
> = (action$, _state$, { auth, logging, userAgent, storage }: Dependencies) => action$.pipe(
  filter(isOfType(HomeActionsTypes.ANON_LOG_IN_AND_DOWNLOAD_URL)),
  tap(_val => logging.startTrace('ANON_LOG_IN_AND_DOWNLOAD_URL', new Date(), 'auth.userId()', auth.isAnon(), userAgent, 60000)),
  mergeMap((_action) => {
    return from(
      auth.logInAnonymously()
      .then(() => storage.getDownloadUrls())
    ).pipe(
      map((downloadUrls) => anonLogInAndDownloadUrlSuccess(downloadUrls)),
      tap(_val => logging.endTrace('ANON_LOG_IN_AND_DOWNLOAD_URL', auth.userId(), auth.isAnon())),
      catchError((err: Error) => of(error(err.message)).pipe(tap(err => logging.endTraceWithError('ANON_LOG_IN_AND_DOWNLOAD_URL', auth.userId(), auth.isAnon(), err.payload!)))),
    )
  }),
);

export const homeLogInEpic: Epic<
  HomeActions,
  HomeActions,
  RootState,
  any
> = (action$, _state$, { auth, logging, userAgent }: Dependencies) => action$.pipe(
  filter(isOfType(HomeActionsTypes.LOG_IN)),
  tap(_val => logging.startTrace('LOG_IN', new Date(), auth.userId(), auth.isAnon(), userAgent, 60000)),
  mergeMap((action) => {
    return from(
      auth.logInEmailAndPassword(
        action.payload.email,
        action.payload.password
      )
    ).pipe(
      mapTo(logInSuccess()),
      tap(_val => logging.endTrace('LOG_IN', auth.userId(), auth.isAnon())),
      // map((result: firebase.auth.UserCredential) => receiveEvents(events)),
      catchError((err: Error) => of(error(err.message)).pipe(tap(err => logging.endTraceWithError('LOG_IN', auth.userId(), auth.isAnon(), err.payload!)))),
    )
  }),
);

export const homeSignUpEpic: Epic<
  HomeActions,
  HomeActions,
  RootState,
  any
> = (action$, _state$, { auth }: Dependencies) => action$.pipe(
  filter(isOfType(HomeActionsTypes.SIGN_UP)), 
  mergeMap((action) => from(
    auth.signUpEmailAndPassword(
      action.payload.email,
      action.payload.password
    )
  ).pipe(
    mapTo(signUpSuccess()),
    // map((result: firebase.functions.HttpsCallableResult) => receiveEvents(events)),
    // TODO: Currently this doesn't tell the user why they can't sign up with email and password. Using static message instead.
    catchError((_err: Error) => of(error("Account may already exist."))),
  )),
);

/*
export const homeLogInEpic: Epic<
  HomeActions,
  HomeActions,
  RootState,
  Dependencies>
= (action$, state$, { auth }: Dependencies) => action$.pipe(
  filter(isOfType(HomeActionsTypes.LOG_IN)), 
  mergeMap((action) => {
    return from(
      auth.signInEmailAndPassword(
        action.payload.email,
        action.payload.password
      )
    ).pipe(
      mapTo(logInSuccess()),
      // map((result: firebase.auth.UserCredential) => receiveEvents(events)),
      catchError((err: Error) =>
        merge(
          of(
            error(err.message),
          ),
          of(
            removeError()
          ).pipe(
            delay(3000)
          )
        )
      ),
    )
  }),
);
*/
