import React, { useEffect }  from 'react';
import { RootState } from '../store';
import { useSelector, useDispatch } from 'react-redux'
import { useState } from 'react';

import routes from '../constants/routes.json'

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Link, Redirect } from 'react-router-dom';
import { anonLogInAndDownloadUrl, logIn, signUp } from './homeActions';
import CircularProgress from '@material-ui/core/CircularProgress';
import Card from '@material-ui/core/Card';
import { Transition } from 'react-transition-group';

const defaultStyle = {
  transition: `opacity 1000ms ease`,
  opacity: 1,
};

const transitionStyles: { [id: string]: React.CSSProperties } = {
  entering: { opacity: 1 },
  entered: { opacity: 0 },
  exiting: { opacity: 0 },
  exited: { opacity: 0 },
};

/*
Normally, AComponent would move right into the entered state without passing
through the entering state on first mount. To change that, we need to add a
prop called appear to the Transition component used in AComponent. We also
want to use a different time duration for each state. We’ll set appear to
100ms and enter and exit to 300ms. Finally, we want to apply the default
and transition styles to the div element returned from the child function of
the Transition Component. AComponent should look like this now:
*/

export default function Home(): JSX.Element {
  const { authenticated } = useSelector((state: RootState) => state.auth);
  const { loading, success, error, downloadUrl } = useSelector((state: RootState) => state.home);

  const dispatch = useDispatch();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  
  const handleEmailChange = (e: any) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e: any) => {
    setPassword(e.target.value);
  }

  useEffect(() => {
    dispatch(anonLogInAndDownloadUrl());
  }, [dispatch]);

  if (authenticated) {
    return <Redirect to={routes.CALENDAR_DAY} />;
  }

  // TODO: https://stackoverflow.com/questions/52637818/fade-in-material-ui-just-disables-the-visibility-of-the-component-how-to-get

  // alert('clicked')
  return (
    <div>
      <h2>Icon Self Tech </h2>
      <p style={{ color: '#000000' }}>Our logo</p>
      <form noValidate autoComplete="off">
        <TextField
          inputProps={{ "data-testid": "home_email" }}
          required
          id="filled-required"
          label="Email"
          variant="filled"
          onChange={handleEmailChange}
        />
        <TextField
          inputProps={{ "data-testid": "home_password" }}
          id="filled-password-input"
          label="Password"
          type="password"
          autoComplete="current-password"
          variant="filled"
          onChange={handlePasswordChange}
        />
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            dispatch(logIn(email, password));
          }}
        >
          Log In 
        </Button>
      </form>
      <Link to={routes.CALENDAR_DAY}>
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            dispatch(signUp(email, password));
          }}
        >
          Sign Up
        </Button>
      </Link>
      <a
        href={downloadUrl} download
      >
        Download for Windows
      </a>
      <p style={{ color: '#000000' }}>Some content1</p>
      <p style={{ color: '#000000' }}>Some content2</p>
      {loading && <CircularProgress
        data-testid = 'home_loading'
        style = {{
          minWidth: '50px',
          maxWidth: '50px',
          minHeight: '50px',
          maxHeight: '50px',
          position: 'absolute',
          bottom: 'calc(50vh - 25px)',
          right: 'calc(50vw - 25px)',
        }}
      />}
      <Transition
        in={success}
        timeout={{
          enter: 3000,
        }}
      >
        {state => (
          <div
            style={{
              ...defaultStyle,
              ...transitionStyles[state]
            }}
          >
            { success && <Card
            data-testid = 'home_success'
              style = {{
                minWidth: 'calc(50vw)',
                maxWidth: 'calc(50vw)',
                position: 'absolute',
                bottom: 'calc(50vh)',
                right: 'calc(25vw)',
              }}
            >
              Successfully signed up.
            </Card> }
          </div>
        )}
      </Transition>
      <Transition
        in={error !== undefined}
        timeout={{
          enter: 3000,
        }}
      >
        {state => (
          <div
            style={{
              ...defaultStyle,
              ...transitionStyles[state]
            }}
          >
            { error && <Card
              data-testid = 'home_error'
              style = {{
                minWidth: 'calc(50vw)',
                maxWidth: 'calc(50vw)',
                position: 'absolute',
                bottom: 'calc(50vh)',
                right: 'calc(25vw)',
              }}
            >
              Error: {error}
            </Card> }
          </div>
        )}
      </Transition>
    </div>
  );
}

// unmountOnExit appear
