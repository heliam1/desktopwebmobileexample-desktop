import { Action } from '@reduxjs/toolkit';

export enum HomeActionsTypes {
  ANON_LOG_IN_AND_DOWNLOAD_URL = 'ANON_LOG_IN_AND_DOWNLOAD_URL',
  ANON_LOG_IN_AND_DOWNLOAD_URL_SUCCESS = 'ANON_LOG_IN_AND_DOWNLOAD_URL_SUCCESS',
  LOG_IN = 'LOG_IN',
  LOG_IN_SUCCESS = 'LOG_IN_SUCCESS',
  SIGN_UP = 'SIGN_UP',
  SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS',
  ERROR = 'ERROR',
}

export interface HomeState {
  loading: boolean,
  error?: string,
  success: boolean,
  downloadUrl: string,
}

export interface AnonLogInAndDownloadUrlAction extends Action {
  type: typeof HomeActionsTypes.ANON_LOG_IN_AND_DOWNLOAD_URL;
}

export interface AnonLogInAndDownloadUrlSuccessAction extends Action {
  type: typeof HomeActionsTypes.ANON_LOG_IN_AND_DOWNLOAD_URL_SUCCESS;
  payload: string;
}
  
export interface LogInAction extends Action {
  type: typeof HomeActionsTypes.LOG_IN;
  payload: {
    email: string,
    password: string
  };
}

export interface LogInSuccessAction extends Action {
  type: typeof HomeActionsTypes.LOG_IN_SUCCESS;
}

export interface SignUpAction extends Action {
  type: typeof HomeActionsTypes.SIGN_UP;
  payload: {
    email: string,
    password: string
  };
}

export interface SignUpSuccessAction extends Action {
  type: typeof HomeActionsTypes.SIGN_UP_SUCCESS;
}

export interface ErrorAction extends Action {
  type: typeof HomeActionsTypes.ERROR;
  payload?: string;
}

export type HomeActions = (
  AnonLogInAndDownloadUrlAction |
  AnonLogInAndDownloadUrlSuccessAction | 
  LogInAction |
  LogInSuccessAction |
  SignUpAction |
  SignUpSuccessAction |
  ErrorAction
);

export function anonLogInAndDownloadUrl(): AnonLogInAndDownloadUrlAction {
  return {
    type: HomeActionsTypes.ANON_LOG_IN_AND_DOWNLOAD_URL,
  };
}

export function anonLogInAndDownloadUrlSuccess(downloadUrls: string): AnonLogInAndDownloadUrlSuccessAction {
  return {
    type: HomeActionsTypes.ANON_LOG_IN_AND_DOWNLOAD_URL_SUCCESS,
    payload: downloadUrls
  };
}

export function logIn(email: string, password: string): LogInAction {
  return {
    type: HomeActionsTypes.LOG_IN,
    payload: {
      email,
      password
    }
  };
}

export function logInSuccess(): LogInSuccessAction {
  return {
    type: HomeActionsTypes.LOG_IN_SUCCESS,
  };
}

export function signUp(email: string, password: string): SignUpAction {
  return {
    type: HomeActionsTypes.SIGN_UP,
    payload: {
      email,
      password
    }
  };
}

export function signUpSuccess(): SignUpSuccessAction {
  return {
    type: HomeActionsTypes.SIGN_UP_SUCCESS,
  };
}

export function error(err: string): ErrorAction {
  return {
    type: HomeActionsTypes.ERROR,
    payload: err
  };
}
