import { HomeState, HomeActions, HomeActionsTypes } from './homeActions';

export const initialState: HomeState = {
  loading: false,
  error: undefined,
  success: false,
  downloadUrl: '#',
}

export function homeReducer(
  state = initialState,
  action: HomeActions
): HomeState {
  switch (action.type) {
    case HomeActionsTypes.ANON_LOG_IN_AND_DOWNLOAD_URL:
      return {
        ...state,
      };
    case HomeActionsTypes.ANON_LOG_IN_AND_DOWNLOAD_URL_SUCCESS:
      return {
        ...state,
        downloadUrl: action.payload,
      };
    case HomeActionsTypes.LOG_IN:
      return {
        ...state,
        loading: true,
        error: undefined,
        success: false,
      };
    // Navigation should show log in success.
    case HomeActionsTypes.LOG_IN_SUCCESS:
      return {
        ...state,
        loading: false,
        error: undefined,
        success: false,
      };
    case HomeActionsTypes.SIGN_UP:
      return {
        ...state,
        loading: true,
        error: undefined,
        success: false,
      };
    // Home UI should show sign up success.
    case HomeActionsTypes.SIGN_UP_SUCCESS:
      return {
        ...state,
        loading: false,
        error: undefined,
        success: true,
      };
    case HomeActionsTypes.ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
        success: false,
      };
    default:
      return state;
  }
}
