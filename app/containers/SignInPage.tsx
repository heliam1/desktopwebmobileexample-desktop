import React from 'react';
import SignIn from '../features/signIn/SignIn';

export default function SignInPage() {
  return <SignIn />;
}
