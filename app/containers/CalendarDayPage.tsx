import React from 'react';
import CalendarDay from '../features/calendarDay/CalendarDay';

export default function CalendarDayPage() {
  return <CalendarDay />;
}
