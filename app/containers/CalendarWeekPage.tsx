import React from 'react';
import CalendarWeek from '../features/calendarWeek/CalendarWeek';

export default function CalendarWeekPage() {
  return <CalendarWeek />;
}
