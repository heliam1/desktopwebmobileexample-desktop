import React from 'react';
import CalendarMonth from '../features/calendarMonth/CalendarMonth';

export default function CalendarMonthPage() {
  return <CalendarMonth />;
}
