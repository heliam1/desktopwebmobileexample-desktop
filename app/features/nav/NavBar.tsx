import React from 'react';
import { Container } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { Link } from 'react-router-dom';
import routes from '../../constants/routes.json';
import HomeIcon from '@material-ui/icons/Home';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import SearchIcon from '@material-ui/icons/Search';
import PersonIcon from '@material-ui/icons/Person';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
// import { useSelector } from 'react-redux';

const useStyles = makeStyles((_theme: Theme) =>
  createStyles({
    topNavBar: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
      border: 1,
      color: 'white',
      minWidth: "500px",
      minHeight: "50px",
      maxHeight: "50px",
      position: "fixed",
      top: 0
    },
    bottomNavBar: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
      border: 1,
      color: 'white',
      minWidth: "500px",
      minHeight: "50px",
      maxHeight: "50px",
      position: "fixed",
      bottom: 0
    },
    box: {
      margin: 0,
      color: 'black',
      minWidth: "500px"

    },
    gridItem: {
      minWidth: "100px",
      maxWidth: "100px",
      background: "#fafafa",
      paddingLeft: "35px",
      paddingRight: "35px",
      paddingTop: "12px",
      paddingBottom: "12px",
      minHeight: "50px",
      maxHeight: "50px"
    },
    icon: {
      minHeight: "25px",
      maxHeight: "25px",
    }
  }),
);

export default function NavBar(props: { top: boolean; }): JSX.Element {
  const classes = useStyles();

  // const {authenticated} = useSelector()

  //let authenticated;

  //if (authenticated === undefined) {
    return(
      <>
        <Container
          className={props.top ? classes.topNavBar : classes.bottomNavBar}
          maxWidth={false}
          disableGutters={true}
        >
          <Box borderBottom={1}
            className={classes.box}
          >
            <Grid
              container
              direction="row"
              justify="center"
            >
              <React.Fragment>
                <Link to={routes.COUNTER}>
                  <Grid
                    data-testid = 'nav_home'
                    item
                    className={classes.gridItem}
                  >
                    <HomeIcon className={classes.icon}/>
                  </Grid>
                </Link>  
                <Link to={routes.CALENDAR_DAY}>
                  <Grid
                    data-testid = 'nav_calendar'
                    item
                    className={classes.gridItem}
                  >
                    <CalendarTodayIcon className={classes.icon}/>
                  </Grid>
                </Link>  
                <Link to={routes.COUNTER}>
                  <Grid
                    data-testid = 'nav_add'
                    item
                    className={classes.gridItem}
                  >
                    <AddCircleOutlineIcon className={classes.icon}/>
                  </Grid>
                </Link> 
                <Link to={routes.COUNTER}>
                  <Grid
                    data-testid = 'nav_search'
                    item
                    className={classes.gridItem}
                  >
                    <SearchIcon className={classes.icon}/>
                  </Grid>
                </Link> 
                <Link to={routes.SETTINGS}>
                  <Grid
                    data-testid = 'nav_profile'
                    item
                    className={classes.gridItem}
                  >
                    <PersonIcon className={classes.icon}/>
                  </Grid>
                </Link> 
              </React.Fragment>
            </Grid>
          </Box>
        </Container>
      </>
    );
  //}
}