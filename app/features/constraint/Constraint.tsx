import React from 'react';
import { Link } from 'react-router-dom';
import routes from '../../constants/routes.json';
import styles from '../../components/Home.css';

export default function SignIn(): JSX.Element {
  return (
    <div className={styles.container} data-tid="container">
      <h2>Sign In</h2>
      <Link to={routes.HOME}>to Home</Link>
    </div>
  );
}
