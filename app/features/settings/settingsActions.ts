import { Action } from '@reduxjs/toolkit';

export enum SettingsActionsTypes {
  SIGN_OUT = 'SIGN_OUT',
  DELETE_ACCOUNT = 'DELETE_ACCOUNT',
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR'
}

export interface SettingsState {
  loading: boolean,
  error?: string
}

export interface SignOutAction extends Action {
  type: typeof SettingsActionsTypes.SIGN_OUT;
}

export interface DeleteAccountAction extends Action {
  type: typeof SettingsActionsTypes.DELETE_ACCOUNT;
}

export interface SuccessAction extends Action {
  type: typeof SettingsActionsTypes.SUCCESS;
}

export interface ErrorAction extends Action {
  type: typeof  SettingsActionsTypes.ERROR;
  payload: string;
}

export type SettingsActions = (
  SignOutAction |
  DeleteAccountAction |
  SuccessAction |
  ErrorAction
);

export function signOut(): SignOutAction {
  return {
    type: SettingsActionsTypes.SIGN_OUT,
  };
}

export function deleteAccount(): DeleteAccountAction {
  return {
    type: SettingsActionsTypes.DELETE_ACCOUNT,
  };
}

export function success(): SuccessAction {
  return {
    type: SettingsActionsTypes.SUCCESS,
  };
}

export function error(error: string): ErrorAction {
  return {
    type: SettingsActionsTypes.ERROR,
    payload: error
  };
}
