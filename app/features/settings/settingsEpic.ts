import { Epic } from 'redux-observable';
import { SettingsActions, SettingsActionsTypes, success, error } from './settingsActions';
import { RootState } from '../../store';
import { filter, mergeMap, map, catchError } from 'rxjs/operators';
import { isOfType } from 'typesafe-actions';
import { from, of } from 'rxjs';

export const signOutEpic: Epic<
  SettingsActions,
  SettingsActions,
  RootState,
  any> // was using Dependencies but created issues for testing
= (action$, _state$, { auth }) => action$.pipe(
  filter(isOfType(SettingsActionsTypes.SIGN_OUT)), 
  mergeMap(() => from(auth.signOut()).pipe(
    map(() => success()),
    catchError((err: Error) => of(error(err.message))),
  )),
);

export const deleteAccountEpic: Epic<
  SettingsActions,
  SettingsActions,
  RootState,
  any> // was using Dependencies but created issues for testing
= (action$, _state$, { auth }) => action$.pipe(
  filter(isOfType(SettingsActionsTypes.DELETE_ACCOUNT)), 
  mergeMap(() => from(auth.deleteAccount()).pipe(
    map(() => success()),
    catchError((err: Error) => of(error(err.message))),
  )),
);
