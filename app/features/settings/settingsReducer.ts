import { SettingsState, SettingsActions, SettingsActionsTypes} from './settingsActions';

export const initialState: SettingsState = {
  loading: false,
  error: undefined
}

export function settingsReducer(
  state = initialState,
  action: SettingsActions
): SettingsState {
  switch (action.type) {
    case SettingsActionsTypes.SIGN_OUT:
      return {
        loading: true,
        error: undefined
      }
    case SettingsActionsTypes.DELETE_ACCOUNT:
      return {
        loading: true,
        error: undefined
      }  
    case SettingsActionsTypes.SUCCESS:
      return {
        loading: false,
        error: undefined
      }
    case SettingsActionsTypes.ERROR:
      return {
        loading: false,
        error: action.payload
      }  
    default:
      return state
  }
}