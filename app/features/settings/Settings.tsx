import React from 'react';
import { RootState } from '../../store';
import { useSelector, useDispatch } from 'react-redux'
// import { useState, useEffect} from 'react';

import NavBar from '../nav/NavBar';
import Button from '@material-ui/core/Button';
import { deleteAccount, signOut } from './settingsActions';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import Card from '@material-ui/core/Card';
import { Transition } from 'react-transition-group';

const defaultStyle = {
  transition: `opacity 1000ms ease`,
  opacity: 1,
};

const transitionStyles: { [id: string]: React.CSSProperties } = {
  entering: { opacity: 1 },
  entered: { opacity: 0 },
  exiting: { opacity: 0 },
  exited: { opacity: 0 },
};


export default function Settings(): JSX.Element {
  const dispatch = useDispatch();
  // const { authenticated } = useSelector((state) => state.auth);
  const { loading, error } = useSelector((state: RootState) => state.settings); // state.events.events for string[]

  return (
    <div>
      <React.Fragment>
        <NavBar top={true}/>
        <div
          style={{
            marginBottom: "50px",
            marginTop: "50px",
            minHeight: "calc(100vh - 100px)"
          }}
        > 
          <Button
            data-testid="settings_sign_out"
            variant="contained"
            color="primary"
            onClick={ () => { dispatch(signOut()); } }
          >
            Sign Out
          </Button>
          <Button
            data-testid="settings_delete_account"
            variant="contained"
            color="primary"
            onClick={ () => { dispatch(deleteAccount()); } }
          >
            Delete Account
          </Button>
        </div>
        { loading && <CircularProgress
          data-testid='settings_loading'
          style = {{
            minWidth: "50px",
            maxWidth: "50px",
            minHeight: "50px",
            maxHeight: "50px",
            position: "absolute",                                          
            bottom: "calc(50vh + 25px)",
            right: "calc(50vw - 25px)",
          }}
        /> }
        <Transition
          in={ error !== undefined }
          timeout={{
            enter: 3000,
          }}
        >
          { state => (
            <div
              style={{
                ...defaultStyle,
                ...transitionStyles[state]
              }}
            >
              { error && <Card
                data-testid='settings_error'
                style = {{
                  minWidth: 'calc(50vw)',
                  maxWidth: 'calc(50vw)',
                  position: 'absolute',
                  bottom: 'calc(50vh)',
                  right: 'calc(25vw)',
                }}
              >
                Error: {error}
              </Card> }
            </div>
          ) }
        </Transition>
        <NavBar top={false}/>
      </React.Fragment>
    </div>
  );
}
