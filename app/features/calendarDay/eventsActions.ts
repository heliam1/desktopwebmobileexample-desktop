import { Action } from '@reduxjs/toolkit';
import Event from '../../models/Event';

export enum EventsActionsTypes {
  LISTEN_EVENTS = 'LISTEN_EVENTS',
  RECEIVE_EVENTS = 'RECEIVE_EVENTS',
  ERROR_EVENTS = 'ERROR_EVENTS'
}

// src/store/chat/types.ts
//export interface Message {
//  user: string
//  message: string
//  timestamp: number
//}

export interface EventsState {
  loading: boolean,
  events: Event[],
  error?: string
}

export interface ListenEventsAction extends Action {
  type: typeof EventsActionsTypes.LISTEN_EVENTS;
  payload: string;
  meta: {
    timestamp: number
  };
}

export interface ReceiveEventsAction extends Action {
  type: typeof EventsActionsTypes.RECEIVE_EVENTS;
  payload: Array<Event>;
}

export interface ErrorEventsAction extends Action {
  type: typeof EventsActionsTypes.ERROR_EVENTS;
  payload: string;
}

// Note that we are using TypeScript's Union Type here to express all possible actions.
export type EventsActions = (
  ListenEventsAction |
  ReceiveEventsAction |
  ErrorEventsAction
);

// TypeScript infers that this function is returning ListEventsAction
export function listenEvents(userId: string, timestamp: number): ListenEventsAction {
  return {
    type: EventsActionsTypes.LISTEN_EVENTS,
    payload: userId,
    meta: {
      timestamp
    }
  };
}

// TypeScript infers that this function is returning ReceiveEventsAction
export function receiveEvents(events: Array<Event>): ReceiveEventsAction {
  return {
    type:  EventsActionsTypes.RECEIVE_EVENTS,
    payload: events
  };
}

export function errorEvents(error: string): ErrorEventsAction {
  return {
    type:  EventsActionsTypes.ERROR_EVENTS,
    payload: error
  };
}
