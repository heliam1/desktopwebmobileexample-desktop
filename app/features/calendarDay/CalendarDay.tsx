import React from 'react';
import { RootState } from '../../store';
import { useSelector, useDispatch } from 'react-redux'
import { useEffect } from 'react';
import NavBar from '../nav/NavBar';
import {
  List,
  AutoSizer,
  CellMeasurer,
  CellMeasurerCache,
} from 'react-virtualized';
import { listenEvents } from './eventsActions';
import { Card, Fab } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import { Transition } from 'react-transition-group';

const defaultStyle = {
  transition: `opacity 1000ms ease`,
  opacity: 1,
};

const transitionStyles: { [id: string]: React.CSSProperties } = {
  entering: { opacity: 1 },
  entered: { opacity: 0 },
  exiting: { opacity: 0 },
  exited: { opacity: 0 },
};


export default function CalendarDay(): JSX.Element {
  const history = useHistory();
  const dispatch = useDispatch();
  // const [activeTab, setActiveTab] = useState(0);
  const { userId } = useSelector((state: RootState) => state.auth);
  const { loading, events, error } = useSelector((state: RootState) => state.events); // state.events.events for string[]

  useEffect(() => {
    // if (retainState) return;
    // setActiveTab(1);
    // dispatch(fetchEvents(fileter, startDate, limit)).then(() => {
    //   setLoadingInitial(false);
    // });
    dispatch(listenEvents(userId!, 1));

    // return function cleanup () {
    //   dispatch({type: RETAIN_STATE})
    // }
  }, [dispatch, userId]); // other dependencies go here

  const cache = React.useRef(
    new CellMeasurerCache({
      fixedWidth: true,
      defaultHeight: 100,
    })
  );

  const getRowHeight = () => {
    return 100; 
  }

  // https://github.com/bvaughn/react-virtualized/blob/master/source/List/List.example.js
  return (
    <div>
      <React.Fragment>
        <NavBar top={true}/>
        <div style={{ marginBottom: "50px",  marginTop: "50px", minHeight: "calc(100vh - 100px)" }} >
          <AutoSizer>
            {({width, height}: {width: number, height: number}) => (
              <List
              width={width}
              height={height}
              rowHeight={getRowHeight()}
              deferredMeasurementCache={cache.current}
              rowCount={events.length}
              rowRenderer={({index, style, parent}) => {
                return (
                  <CellMeasurer
                    style={{ ...style }}
                    key={events[index].id}
                    cache={cache.current}
                    parent={parent}
                    columnIndex={0}
                    rowIndex={index}
                  >
                    <div
                      style={{paddingBottom: "10px", paddingRight:"50px",}}
                    >
                      <Link to={`/event/${events[index].id}`}>
                        <Card
                        style={{
                          ...style,
                          marginLeft:"10px",
                          
                          height:"90px"
                        }}
                        >
                          <h2>{events[index].id}</h2>
                          <p>{events[index].name}</p>
                        </Card>
                      </Link>
                    </div>
                  </CellMeasurer>
                );
              }}
              />
            )}
          </AutoSizer>
        </div>
        {loading && <CircularProgress
          data-testid = 'calendar_day_loading'
          style = {{
            minWidth: "50px",
            maxWidth: "50px",
            minHeight: "50px",
            maxHeight: "50px",
            position: "absolute",                                          
            bottom: "calc(50vh + 25px)",
            right: "calc(50vw - 25px)",
          }}
        />}
        <Transition
          in={error !== undefined}
          timeout={{
            enter: 3000,
          }}
        >
          {state => (
            <div
              style={{
                ...defaultStyle,
                ...transitionStyles[state]
              }}
            >
              { error && <Card
                data-testid = 'calendar_day_error'
                style = {{
                  minWidth: 'calc(50vw)',
                  maxWidth: 'calc(50vw)',
                  position: 'absolute',
                  bottom: 'calc(50vh)',
                  right: 'calc(25vw)',
                }}
              >
                Error: {error}
              </Card> }
            </div>
          )}
        </Transition>
        <NavBar top={false}/>
        <Fab
          color="primary"
          data-testid = 'calendar_day_fab'
          aria-label="add"
          style={{
            position: 'absolute',                                          
            bottom: 120,                                                    
            right: 50,
          }}
          onClick={() => { history.push('/event/newEvent') }}
        >
          <AddIcon />
        </Fab>
      </React.Fragment>
    </div>
  );
}
