import { Epic } from 'redux-observable';
import { EventsActions, EventsActionsTypes, receiveEvents, errorEvents } from './eventsActions';
import { RootState, Dependencies } from '../../store';
import { filter, mergeMap, map, catchError } from 'rxjs/operators';
import { isOfType } from 'typesafe-actions';
import Event from '../../models/Event';
import { of } from 'rxjs';

export const eventsEpic: Epic<
  EventsActions,
  EventsActions,
  RootState,
  Dependencies>
= (action$, _state$, { remoteDb }) => action$.pipe(
  filter(isOfType(EventsActionsTypes.LISTEN_EVENTS)), 
  mergeMap((action) => remoteDb.listenEvents(action.payload).pipe(
    map((events: Event[]) => receiveEvents(events)),
    catchError((error: Error) => of(errorEvents(error.message))),
  )),
);

// put your second epic here?
//     .
//orrr filter(action => action.type === 'PING'), action$.pipe(ofType(FIRST, SECOND, THIRD)) // FIRST or SECOND or THIRD
