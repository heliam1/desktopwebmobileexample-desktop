import { EventsState, EventsActions, EventsActionsTypes} from './eventsActions';
import Event from '../../models/Event';

export const initialState: EventsState = {
  loading: false,
  events: new Array<Event>(),
  error: undefined
}

export function eventsReducer(
  state = initialState,
  action: EventsActions
): EventsState {
  switch (action.type) {
    case EventsActionsTypes.LISTEN_EVENTS:
      return {
        ...state,
        loading: true,
        error: undefined,
      }
    case EventsActionsTypes.RECEIVE_EVENTS:
      return {
        loading: false,
        events: action.payload,
        error: undefined
      }
    case EventsActionsTypes.ERROR_EVENTS:
      return {
        ...state,
        loading: false,
        error: action.payload
      }  
    default:
      return state
  }
}
