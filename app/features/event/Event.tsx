import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import NavBar from '../nav/NavBar';
import { Button, Fab, Grid, TextField } from '@material-ui/core';
import DoneIcon from '@material-ui/icons/Done';
// import { useHistory } from 'react-router-dom';
// import { useState } from 'react';
import { RootState } from '../../store';
import { useDispatch, useSelector } from 'react-redux';
import { cleanUp, deleteEvent, getEvent, nameChanged, newEvent, saveEvent } from './eventActions';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import Card from '@material-ui/core/Card';
import { Transition } from 'react-transition-group';

const defaultStyle = {
  transition: `opacity 1000ms ease`,
  opacity: 1,
};

const transitionStyles: { [id: string]: React.CSSProperties } = {
  entering: { opacity: 1 },
  entered: { opacity: 0 },
  exiting: { opacity: 0 },
  exited: { opacity: 0 },
};

//interface ParamTypes {
//  id: string;
//}

export default function Event(): JSX.Element {
  // const history = useHistory();
  const dispatch = useDispatch();
  // const { id } = useParams<ParamTypes>();
  const { id } = useParams() as {
    id: string;
  }
  const { userId } = useSelector((state: RootState) => state.auth);
  const { loading, event, error } = useSelector((state: RootState) => state.event);
  
  const handleNameChange = (e: any) => {
    dispatch(nameChanged(e.target.value));
  };

  useEffect(() => {
    if (id === 'newEvent') {
      dispatch(newEvent());
    } else {
      dispatch(getEvent(userId!, id));
    }
    
    return () => {
      dispatch(cleanUp());
    }
  }, [id, dispatch, userId]);

  return (
    <div>
      <React.Fragment>
        <NavBar top={true}/>
        <div style={{marginBottom: "50px",  marginTop: "50px", minHeight: "calc(100vh - 100px)"}} >
          <Grid
            container
            direction="row"
          >
            <React.Fragment>
              <Grid item>
                <h2
                  data-testid = 'event_id'
                  style={{
                    paddingTop: "10px",
                  }}
                >
                  {event.id}
                </h2>
              </Grid>
              { !(id === 'newEvent' || loading) && <Grid item>
                <Button
                  data-testid = 'event_delete'
                  variant="contained"
                  style={{
                    paddingTop: "0px",
                    marginTop:"15px",
                    marginLeft:"20px",
                    paddingLeft: "10px",
                    minHeight: "30px",
                    maxHeight: "30px",
                    color:'black',
                  }}
                  onClick={() => { dispatch(deleteEvent(userId!, id)) }}
                >
                  <DeleteForeverIcon
                    style={{
                      paddingTop: "5px",
                      paddingLeft: "5px",
                      color:'black',
                    }}
                  />
                </Button>
              </Grid> }
            </React.Fragment>
          </Grid>
          <TextField
            inputProps={{ "data-testid": "event_name" }}
            required
            id="filled-required"
            label="Name"
            variant="filled"
            onChange={handleNameChange}
            value={event.name}
          />
        </div>
        {loading && <CircularProgress
          data-testid = 'event_loading'
          style = {{
            minWidth: "50px",
            maxWidth: "50px",
            minHeight: "50px",
            maxHeight: "50px",
            position: "absolute",                                          
            bottom: "calc(50vh + 25px)",
            right: "calc(50vw - 25px)",
          }}
        />}
        <Transition
          in={error !== undefined}
          timeout={{
            enter: 3000,
          }}
        >
          {state => (
            <div
              style={{
                ...defaultStyle,
                ...transitionStyles[state]
              }}
            >
              { error && <Card
                data-testid = 'event_error'
                style = {{
                  minWidth: 'calc(50vw)',
                  maxWidth: 'calc(50vw)',
                  position: 'absolute',
                  bottom: 'calc(50vh)',
                  right: 'calc(25vw)',
                }}
              >
                Error: {error}
              </Card> }
            </div>
          )}
        </Transition>
        <NavBar top={false}/>
        <Fab
          variant="extended"
          color="primary"
          aria-label="add"
          data-testid = 'event_save'
          style={{
            position: 'absolute',                                          
            bottom: 120,                                                    
            left: 0,
            right: 0, 
            marginLeft: "auto",
            marginRight: "auto", 
            width: "150px"
          }}
          onClick={() => dispatch(saveEvent(event))}
        >
          <DoneIcon />
          Save
        </Fab>
      </React.Fragment>
    </div>
  );
}
