import { Epic } from 'redux-observable';
import { EventActions, EventActionsTypes, saveError, saveSuccess, getError, getSuccess, deleteSuccess, deleteError } from './eventActions';
import { RootState, Dependencies } from '../../store';
import { filter, mergeMap, map, catchError, tap } from 'rxjs/operators';
import { isOfType } from 'typesafe-actions';
import { from, of } from 'rxjs';
import routes from '../../constants/routes.json'

import Event from '../../models/Event';

export const saveEventEpic: Epic<
  EventActions,
  EventActions,
  RootState,
  Dependencies>
= (action$, state$, { remoteDb, nav }) => action$.pipe(
  filter(isOfType(EventActionsTypes.SAVE_EVENT)), 
  mergeMap((_action) => from(remoteDb.saveEvent(state$.value.auth.userId!, state$.value.event.event)).pipe(
    map(() => saveSuccess()),
    // TODO: goBack() doesn't seem to work. we can use state to figure where we should go.
    catchError((error: Error) => of(saveError(error.message))),
    tap((val) => {
      if (val.type !== 'SAVE_ERROR')
        return nav.replace(routes.CALENDAR_DAY)
    }),
  )),
);

export const getEventEpic: Epic<
  EventActions,
  EventActions,
  RootState,
  Dependencies>
= (action$, _state$, { remoteDb }) => action$.pipe(
  filter(isOfType(EventActionsTypes.GET_EVENT)), 
  mergeMap((action) => from(remoteDb.getEvent(action.payload.userId, action.payload.eventId)).pipe(
    map((event: Event) => getSuccess(event)),
    catchError((error: Error) => of(getError(error.message))),
  )),
);

export const deleteEventEpic: Epic<
  EventActions,
  EventActions,
  RootState,
  Dependencies>
= (action$, _state$, { remoteDb, nav }) => action$.pipe(
  filter(isOfType(EventActionsTypes.DELETE_EVENT)),
  mergeMap((action) => from(remoteDb.deleteEvent(action.payload.userId, action.payload.eventId)).pipe(
    map(() => deleteSuccess()),
    // TODO: goBack() doesn't seem to work. we can use state to figure where we should go.
    catchError((error: Error) => of(deleteError(error.message))),
    tap((val) => {
      if (val.type !== 'DELETE_ERROR')
        return nav.replace(routes.CALENDAR_DAY)
    }),
  )),
);

//export const nameChangedEpic: Epic<
//  EventActions,
//  EventActions,
//  RootState,
//  Dependencies>
//= (action$, state$, { remoteDb }) => action$.pipe(
//  filter(isOfType(EventActionsTypes.NAMED_CHANGED)), 
//  mergeMap((action) => remoteDb.listenEvents(action.payload).pipe(
//    map((events: Event[]) => receiveEvents(events)),
//    catchError((error: Error) => of(errorEvents(error.message))),
//  )),
//);
