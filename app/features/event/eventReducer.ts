import { EventState, EventActions, EventActionsTypes} from './eventActions';

export const initialState: EventState = {
  loading: false,
  event: { id: 'loading', name: 'loading' },
  error: undefined
}

export function eventReducer(
  state = initialState,
  action: EventActions
): EventState {
  switch (action.type) {
    case EventActionsTypes.NEW_EVENT:
      return {
        loading: false,
        event: {
          id: 'newEvent',
          name: '',
        },
        error: undefined
      }
    case EventActionsTypes.NAMED_CHANGED:
      return {
        ...state,
        event: {
          ...state.event,
          name: action.payload
        },
      }
    case EventActionsTypes.SAVE_EVENT:
      return {
        loading: true,
        event: action.payload,
        error: undefined
      }
    case EventActionsTypes.SAVE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: undefined
      }  
    case EventActionsTypes.SAVE_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      }  
    case EventActionsTypes.GET_EVENT:
      return {
        ...state,
        loading: true,
        error: undefined
      }
    case EventActionsTypes.GET_SUCCESS:
      return {
        loading: false,
        event: {
          id: action.payload.id,
          name: action.payload.name,
        },
        error: undefined
      }  
    case EventActionsTypes.GET_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      }
    case EventActionsTypes.CLEAN_UP:
      return initialState;
    case EventActionsTypes.DELETE_EVENT:
      return {
        ...state,
        loading: true,
        error: undefined
      }
    case EventActionsTypes.DELETE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: undefined
      }  
    case EventActionsTypes.DELETE_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      }  
    default:
      return state
  }
}
