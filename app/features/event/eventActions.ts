import { Action } from '@reduxjs/toolkit';
import Event from '../../models/Event';

export enum EventActionsTypes {
  NEW_EVENT = 'NEW_EVENT',
  NAMED_CHANGED = 'NAME_CHANGED',
  SAVE_EVENT = 'SAVE_EVENT',
  SAVE_SUCCESS = 'SAVE_SUCCESS',
  SAVE_ERROR = 'SAVE_ERROR',
  GET_EVENT = 'GET_EVENT',
  GET_SUCCESS = 'GET_SUCCESS',
  GET_ERROR = 'GET_ERROR',
  DELETE_EVENT = 'DELETE_EVENT',
  DELETE_SUCCESS = 'DELETE_SUCCESS',
  DELETE_ERROR = 'DELETE_ERROR',
  CLEAN_UP = 'CLEAN_UP',
}

export interface EventState {
  loading: boolean,
  event: Event,
  error?: string
}

export interface NewEventAction extends Action {
  type: typeof EventActionsTypes.NEW_EVENT;
}

export interface NameChangedAction extends Action {
  type: typeof EventActionsTypes.NAMED_CHANGED;
  payload: string
}

export interface SaveEventAction extends Action {
  type: typeof EventActionsTypes.SAVE_EVENT;
  payload: Event;
}

export interface SaveSuccessAction extends Action {
  type: typeof EventActionsTypes.SAVE_SUCCESS;
}

export interface SaveErrorAction extends Action {
  type: typeof EventActionsTypes.SAVE_ERROR;
  payload: string
}

export interface GetEventAction extends Action {
  type: typeof EventActionsTypes.GET_EVENT;
  payload: {
    userId: string,
    eventId: string,
  };
}

export interface GetSuccessAction extends Action {
  type: typeof EventActionsTypes.GET_SUCCESS;
  payload: Event;
}

export interface GetErrorAction extends Action {
  type: typeof EventActionsTypes.GET_ERROR;
  payload: string
}

export interface DeleteEventAction extends Action {
  type: typeof EventActionsTypes.DELETE_EVENT;
  payload: {
    userId: string,
    eventId: string,
  };
}

export interface DeleteSuccessAction extends Action {
  type: typeof EventActionsTypes.DELETE_SUCCESS;
}

export interface DeleteErrorAction extends Action {
  type: typeof EventActionsTypes.DELETE_ERROR;
  payload: string
}

export interface CleanUpAction extends Action {
  type: typeof EventActionsTypes.CLEAN_UP;
}

export type EventActions = (
  NewEventAction |
  NameChangedAction |
  SaveEventAction |
  SaveSuccessAction |
  SaveErrorAction |
  GetEventAction |
  GetSuccessAction |
  GetErrorAction |
  DeleteEventAction |
  DeleteSuccessAction |
  DeleteErrorAction |  
  CleanUpAction
);

export function newEvent(): NewEventAction {
  return {
    type: EventActionsTypes.NEW_EVENT,
  };
}

export function nameChanged(name: string): NameChangedAction {
  return {
    type: EventActionsTypes.NAMED_CHANGED,
    payload: name,
  };
}

export function saveEvent(event: Event): SaveEventAction {
  return {
    type: EventActionsTypes.SAVE_EVENT,
    payload: event
  };
}

export function saveSuccess(): SaveSuccessAction {
  return {
    type: EventActionsTypes.SAVE_SUCCESS
  };
}

export function saveError(error: string): SaveErrorAction {
  return {
    type: EventActionsTypes.SAVE_ERROR,
    payload: error,
  };
}

export function getEvent(userId: string, eventId: string): GetEventAction {
  return {
    type: EventActionsTypes.GET_EVENT,
    payload: {
      userId,
      eventId,
    }
  };
}

export function getSuccess(event: Event): GetSuccessAction {
  return {
    type: EventActionsTypes.GET_SUCCESS,
    payload: event,
  };
}

export function getError(error: string): GetErrorAction {
  return {
    type: EventActionsTypes.GET_ERROR,
    payload: error,
  };
}

export function deleteEvent(userId: string, eventId: string): DeleteEventAction {
  return {
    type: EventActionsTypes.DELETE_EVENT,
    payload: {
      userId,
      eventId,
    }
  };
}

export function deleteSuccess(): DeleteSuccessAction {
  return {
    type: EventActionsTypes.DELETE_SUCCESS,
  };
}

export function deleteError(error: string): DeleteErrorAction {
  return {
    type: EventActionsTypes.DELETE_ERROR,
    payload: error,
  };
}


export function cleanUp(): CleanUpAction {
  return {
    type: EventActionsTypes.CLEAN_UP,
  };
}
