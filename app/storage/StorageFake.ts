import { Storage } from './Storage';

export class StorageFake implements Storage {
  getDownloadUrls(): Promise<string> {
    return Promise.resolve('https://fakeUrl.com');
  }
}