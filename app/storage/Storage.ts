export interface Storage {
  getDownloadUrls(): Promise<string>;
}