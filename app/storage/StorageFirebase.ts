import { Storage } from './Storage';
import '@firebase/storage'

export class StorageFirebase implements Storage {
  readonly storage: firebase.storage.Storage;

  constructor(dependency: firebase.app.App) {
    this.storage = dependency.storage();
  }

  getDownloadUrls(): Promise<string> {
    return this.storage.ref('desktopwebmobileexample-desktop Setup 1.0.0.exe').getDownloadURL();
  }
}