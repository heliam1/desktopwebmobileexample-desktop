import { Log } from "../Log";

export interface ClientLoggingDb {
  writeLog(log: Log): void;
}