import '@firebase/database'
import { Log } from "../Log";
import { ClientLoggingDb } from "./ClientLoggingDb";

export class ClientLoggingDbImpl implements ClientLoggingDb {
  readonly rtdb: firebase.database.Database;

  constructor(dependency: firebase.app.App) {
    this.rtdb = dependency.database();
  }

  writeLog(log: Log) {
    this.rtdb
    .ref(`logs/${log.getStart().valueOf()}_${log.getName()}_${log.getUserId()}`)
    .set({
      name: log.getName(),
      start: log.getStart().valueOf(),
      startIso: log.getStartIso(),
      latency: log.getLatency(),
      userId: log.getUserId(),
      anon: log.getAnon(),
      userAgent: log.getUserAgent(),
      unread: log.getUnread(),
      attempts: log.getAttempts(),
      error: log.getError(),
    })
    .catch((err) => {
      console.log(`Failed to log: ${err}`);
      console.log(`logs/${log.getStart().valueOf()}_${log.getName()}_${log.getUserId()}`);
      console.log(log.getStart().valueOf());
      console.log(log);
    });
  }
}
