export interface ClientLogging {
  startTrace(nameOfTrace: string, startTime: Date, userId: string, anon: boolean, userAgent: string, timeoutDurationMs: number): void;

  endTrace(nameOfTrace: string, userId: string, anon: boolean): boolean;
  
  endTraceWithError(nameOfTrace: string, userId: string, anon: boolean, error: string): boolean;
}
