export class Log {
  readonly name: string;
  readonly start: Date;
  readonly startIso: string;
  readonly latency: number;
  readonly userId: string;
  readonly anon: boolean;
  readonly userAgent: string;
  readonly unread: boolean;
  readonly attempts: number;
  readonly error: string;

  constructor(
    name: string,
    start: Date,
    startIso: string,
    latency: number,
    userId: string,
    anon: boolean,
    userAgent: string,
    unread: boolean,
    attempts: number,
    error: string,
  ) {
    this.name = name;
    this.start = start;
    this.startIso = startIso;
    this.latency = latency;
    this.userId = userId;
    this.anon = anon;
    this.userAgent = userAgent;
    this.unread = unread;
    this.attempts = attempts;
    if (error.length > 32) {
      this.error = error.substring(0, 32);
    } else {
      this.error = error;
    }
  }

  getName(): string { return this.name; }
  getStart(): Date { return this.start; }
  getStartIso(): string { return this.startIso; }
  getLatency(): number { return this.latency; }
  getUserId(): string { return this.userId; }
  getAnon(): boolean { return this.anon; }
  getUserAgent(): string { return this.userAgent; }
  getUnread(): boolean { return this.unread; }
  getAttempts(): number { return this.attempts; }
  getError(): string { return this.error; }
}

export function dateFromIso(s: string): Date {
  var b = s.split(/\D+/);
  return new Date(Date.UTC(Number(b[0]), Number(b[1]), Number(b[2]), Number(b[3]), Number(b[4]), Number(b[5]), Number(b[6])));
}
