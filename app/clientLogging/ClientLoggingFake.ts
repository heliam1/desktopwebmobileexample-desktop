import { ClientLogging } from "./ClientLogging";

export class ClientLoggingFake implements ClientLogging {
  startTrace(_nameOfTrace: string, _startTime: Date, _userId: string, _anon: boolean, _userAgent: string, _timeoutDurationMs: number): void {
    return;
  }

  endTrace(_nameOfTrace: string, _userId: string, _anon: boolean): boolean {
    return true;
  }

  endTraceWithError(_nameOfTrace: string, _userId: string, _anon: boolean, _error: string): boolean {
    return true;
  }
}
