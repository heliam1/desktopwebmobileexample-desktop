import { ClientLogging } from './ClientLogging';
import { ClientLoggingDb } from './clientLoggingDb/ClientLoggingDb';
import { Log } from './Log';

export class ClientLoggingImpl implements ClientLogging {

  readonly db: ClientLoggingDb;

  readonly activeTraces: Map<string,any> = new Map();

  constructor(dependency: ClientLoggingDb) {
    this.db = dependency;
  }

  startTrace(nameOfTrace: string, startTime: Date, userId: string, anon: boolean, userAgent: string, timeoutDurationMs: number) {
    console.log(`Start trace ${nameOfTrace}`);
    const trace: any = this.activeTraces.get(nameOfTrace);
    if (trace !== undefined) {
      this.activeTraces.set(nameOfTrace, { userId: trace.userId, anon: trace.anon, startTime: trace.startTime, userAgent: trace.userAgent, attempts: trace.attempts++ });
    } else {
      this.activeTraces.set(nameOfTrace, { userId, anon, startTime, userAgent, attempts: 1 });

      setTimeout(() => {
        const timedOutTrace: any = this.activeTraces.get(nameOfTrace);
        if (timedOutTrace !== undefined) {
          this.activeTraces.delete(nameOfTrace);
          const log = new Log(nameOfTrace, startTime, startTime.toISOString(), -1, userId, anon, userAgent, true, timedOutTrace.attempts, '');
          
          this.db.writeLog(log);
        }
      }, timeoutDurationMs)
    }
  }
      
  endTrace(nameOfTrace: string, userId: string, anon: boolean): boolean {
    console.log(`End trace ${nameOfTrace}`);
    const trace: any = this.activeTraces.get(nameOfTrace);

    if (trace !== undefined) {
      this.activeTraces.delete(nameOfTrace);

      const latency = new Date().valueOf() - trace.startTime.valueOf();
      const log = new Log(
        nameOfTrace,
        trace.startTime,
        trace.startTime.toISOString(),
        latency,
        userId,
        anon,
        trace.userAgent,
        true,
        trace.attempts,
        '',
      );
      
      this.db.writeLog(log);
      return true;
    }
    return false;
  }

  endTraceWithError(nameOfTrace: string, userId: string, anon: boolean, error: string): boolean {
    const trace: any = this.activeTraces.get(nameOfTrace);

    if (trace !== undefined) {
      this.activeTraces.delete(nameOfTrace);

      const log = new Log(
        nameOfTrace,
        trace.startTime,
        trace.startTime.toISOString(),
        -1,
        userId,
        anon,
        trace.userAgent,
        true,
        trace.attempts,
        error
      );
      
      this.db.writeLog(log);
      return true;
    }
    return false;
  }
}
