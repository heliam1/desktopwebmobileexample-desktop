import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { History } from 'history';
// eslint-disable-next-line import/no-cycle
import { authReducer } from './auth/authReducer';
import counterReducer from './features/counter/counterSlice';
import { eventsReducer } from './features/calendarDay/eventsReducer';
import { homeReducer } from './components/homeReducer';
import { settingsReducer } from './features/settings/settingsReducer';
import { eventReducer } from './features/event/eventReducer';

export default function createRootReducer(history: History) {
  return combineReducers({
    router: connectRouter(history),
    auth: authReducer,
    home: homeReducer,
    counter: counterReducer,
    events: eventsReducer,
    settings: settingsReducer,
    event: eventReducer,
  });
}
