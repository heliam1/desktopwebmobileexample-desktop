import { ReplaySubject, Observable } from 'rxjs';
import Event from '../models/Event';

export interface RemoteDb {
  eventsSub?: () => void;
  eventsStreamController?: ReplaySubject<Array<Event>>;

  listenEvents(userId: String): Observable<Array<Event>>;
  stopListeningEvents(): void;
  getEvent(userId: string, eventId: string): Promise<Event>;
  saveEvent(userId: string, event: Event): Promise<void>;
  deleteEvent(userId: string, id: string): Promise<void>;
  

  createNewEventKey(userId: string): string;
  createNewRepeatKey(userId: string): string;
  createNewConstraintKey(userId: string): string;

  dispose(): void;
}