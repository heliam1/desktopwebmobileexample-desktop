import { ReplaySubject, Observable, from } from 'rxjs';
import { RemoteDb } from './RemoteDb'
import Event from '../models/Event';

export class RemoteDbFake implements RemoteDb {

  eventsSub?: () => void;
  eventsStreamController?: ReplaySubject<Array<Event>>;
  
  listenEvents(_userId: string): Observable<Array<Event>> {
    if (!(this.eventsStreamController)) {
      this.eventsStreamController = new ReplaySubject(1);
    }

    const events = [
      {
        id: "1",
        name: "name1",
      },
      {
        id: "2",
        name: "name2",
      }
    ]

    // this.eventsStreamController!.next(events);
    // return this.eventsStreamController!.pipe();
    
    return from(Promise.resolve(events));
  }

  stopListeningEvents(): void {
    if (this.eventsSub) {
      this.eventsSub();
    }
    this.eventsStreamController?.complete();
    this.eventsStreamController = undefined;
    this.eventsSub = undefined;
  }

  saveEvent(_userId: string, _event: Event): Promise<void> {
    return Promise.resolve();
  }

  getEvent(_userId: string, eventId: string): Promise<Event> {
    return Promise.resolve({ id: eventId, name: 'getEventName'});
  }

  deleteEvent(_userId: string, _id: string): Promise<void> {
    return Promise.resolve();
  }

  createNewEventKey(_userId: string): string {
    return "non random string";
  }

  createNewRepeatKey(_userId: string): string {
    return "non random string";
  }

  createNewConstraintKey(_userId: string): string {
    return "non random string";
  }

  dispose(): void {
    console.log('RemoteDbFake-dispose()');
    if (this.eventsSub) {
      this.eventsSub();
    }
    this.eventsStreamController?.complete();
  }
}