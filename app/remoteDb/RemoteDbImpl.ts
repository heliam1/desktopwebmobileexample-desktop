import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { ReplaySubject, Observable } from 'rxjs';
import { RemoteDb } from './RemoteDb';
import Event from '../models/Event';

export class RemoteDbImpl implements RemoteDb {

  eventsSub?: () => void;
  eventsStreamController?: ReplaySubject<Array<Event>>;
  readonly firestore: firebase.firestore.Firestore;

  constructor(dependency: firebase.app.App) {
    this.firestore = dependency.firestore();
  }
  
  listenEvents(userId: string): Observable<Array<Event>> {
    if (!(this.eventsStreamController)) {
      this.eventsStreamController = new ReplaySubject(1);
    }
    if (!(this.eventsSub)) {
      this.eventsSub = this.firestore
      .collection('users')
      .doc(userId)
      .collection('events')
      .orderBy('name')
      .onSnapshot((qs: firebase.firestore.QuerySnapshot) => {
        const events: Array<Event> = [];
        qs.forEach((doc) => {
          events.push(
            {
              id: doc.id,
              name: doc.data().name
            }
          );
        });

        this.eventsStreamController!.next(events);
      }, (error: Error) => {
        console.log("error has occurred");
        console.log(error);
        this.eventsStreamController!.error(error);
        this.eventsSub!();
        this.eventsStreamController = undefined;
        this.eventsSub = undefined;
      });
    }
    return this.eventsStreamController!.pipe();
  }

  stopListeningEvents(): void {
    if (this.eventsSub) {
      this.eventsSub();
    }
    this.eventsStreamController?.complete();
    this.eventsStreamController = undefined;
    this.eventsSub = undefined;
  }

  saveEvent(userId: string, event: Event): Promise<void> {
    return this.firestore
    .collection('users')
    .doc(userId)
    .collection('events')
    .doc(event.id === 'newEvent' ? this.createNewEventKey(userId) : event.id)
    .set({name: event.name});
  }

  getEvent(userId: string, eventId: string): Promise<Event> {
    return this.firestore
    .collection('users')
    .doc(userId)
    .collection('events')
    .doc(eventId)
    .get()
    .then((ds: firebase.firestore.DocumentSnapshot) => {
      if (!ds.exists) {
        Promise.reject(new Error('Document does not exist'));
      }
      return {
        id: ds.id,
        name: ds.data()!.name
      }
    });
  }

  deleteEvent(userId: string, id: string): Promise<void> {
    return this.firestore
    .collection('users')
    .doc(userId)
    .collection('events')
    .doc(id)
    .delete();
  }

  createNewEventKey(userId: string): string {
    return this.firestore
    .collection('users')
    .doc(userId)
    .collection('events')
    .doc()
    .id;
  }

  createNewRepeatKey(userId: string): string {
    return this.firestore
    .collection('users')
    .doc(userId)
    .collection('repeats')
    .doc()
    .id;
  }

  createNewConstraintKey(userId: string): string {
    return this.firestore
    .collection('users')
    .doc(userId)
    .collection('constraints')
    .doc()
    .id;
  }

  dispose(): void {
    console.log('RemoteDbImpl-dispose()');
    if (this.eventsSub) {
      this.eventsSub();
    }
    this.eventsStreamController?.complete();
  }
}
