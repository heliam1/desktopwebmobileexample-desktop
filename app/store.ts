import { configureStore, getDefaultMiddleware, Action } from '@reduxjs/toolkit';
import { createHashHistory, History } from 'history';
import { routerMiddleware } from 'connected-react-router';
import { createLogger } from 'redux-logger';
import { ThunkAction } from 'redux-thunk';
// eslint-disable-next-line import/no-cycle
import createRootReducer from './rootReducer';
import createRootEpic from './rootEpic';

import { firebase } from '@firebase/app'
import { Auth } from './auth/Auth';
import { RemoteDb } from './remoteDb/RemoteDb';
import { AuthImpl } from './auth/AuthImpl'
import { RemoteDbImpl } from './remoteDb/RemoteDbImpl';
import { ClientLogging } from './clientLogging/ClientLogging';
import { ClientLoggingImpl } from './clientLogging/ClientLoggingImpl';
import { ClientLoggingDbImpl } from './clientLogging/clientLoggingDb/ClientLoggingDbImpl';
import { Storage } from './storage/Storage'
import { StorageFirebase } from 'storage/StorageFirebase';
import { createEpicMiddleware } from 'redux-observable';

import config from './firebaseConfigGenerated.json';

export const history = createHashHistory();
const rootReducer = createRootReducer(history);
export type RootState = ReturnType<typeof rootReducer>;

const router = routerMiddleware(history);
const middleware = [...getDefaultMiddleware()];

export type Dependencies = {
  nav: History,
  auth: Auth,
  remoteDb: RemoteDb,
  logging: ClientLogging,
  storage: Storage,
  userAgent: string,
};

let app;
let auth: Auth;
let remoteDb: RemoteDb;
let logging: ClientLogging;
let storage: Storage;

// TODO: consider using fakes or emulator here
if (config.projectId.includes("dev")) {
  app = firebase.initializeApp(config);
  auth = new AuthImpl(app as unknown as firebase.app.App);
  remoteDb = new RemoteDbImpl(app as unknown as firebase.app.App)
  logging = new ClientLoggingImpl(new ClientLoggingDbImpl(app as unknown as firebase.app.App));
  storage = new StorageFirebase(app as unknown as firebase.app.App);
} else {
  app = firebase.initializeApp(config);
  auth = new AuthImpl(app as unknown as firebase.app.App);
  remoteDb = new RemoteDbImpl(app as unknown as firebase.app.App)
  logging = new ClientLoggingImpl(new ClientLoggingDbImpl(app as unknown as firebase.app.App));
  storage = new StorageFirebase(app as unknown as firebase.app.App);
}

// TODO: read a config file that contains whether we're chrome, macOS, Windows
// if chrome/browser, can use SO to further identify useragent

const dependencies: Dependencies = {
  nav: history,
  auth,
  remoteDb: remoteDb,
  logging: logging,
  storage: storage,
  userAgent: config.userAgent,
}

const epicMiddleware = createEpicMiddleware({
  dependencies
});
middleware.push(epicMiddleware);
middleware.push(router);

const excludeLoggerEnvs = ['test', 'production'];
const shouldIncludeLogger = !excludeLoggerEnvs.includes(
  process.env.NODE_ENV || ''
);

if (shouldIncludeLogger) {
  const logger = createLogger({
    level: 'info',
    collapsed: true,
  });
  middleware.push(logger);
}

export const configuredStore = (initialState?: RootState) => {
  // Create Store
  const store = configureStore({
    reducer: rootReducer,
    middleware,
    preloadedState: initialState,
  });

  if (process.env.NODE_ENV === 'development' && module.hot) {
    module.hot.accept(
      './rootReducer',
      // eslint-disable-next-line global-require
      () => store.replaceReducer(require('./rootReducer').default)
    );
  }
  epicMiddleware.run(createRootEpic());

  auth.activateAuthStateListener(store);

  return store;
};
export type Store = ReturnType<typeof configuredStore>;
export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>;
