import { EnhancedStore } from '@reduxjs/toolkit';
import 'firebase/auth';
import 'firebase/functions';
import { Auth } from './Auth'

export class AuthFake implements Auth {
  logInAnonymously(): Promise<void> {
    return Promise.resolve();
  }

  isAnon(): boolean {
    return false;
  }

  signUpEmailAndPassword(_email: string, _password: string): Promise<void> {
    return Promise.resolve();
  }

  logInEmailAndPassword(_email: string, _password: string): Promise<void> {
    return Promise.resolve();
  }

  signOut(): Promise<void> {
    return Promise.resolve();
  }

  sendPasswordResetEmail(_email: string): Promise<void> {
    return Promise.resolve();
  }

  userEmail(): string | null {
    return 'example@example.com';
  }

  userId(): string {
    return 'exampleId';
  }

  updateUserEmail(_email: string): Promise<void> {
    return Promise.resolve();
  }

  deleteAccount(): Promise<void> {
    return Promise.resolve().then(() => this.signOut());
  }

  // TODO: consider auth epic that is utilized by root component
  activateAuthStateListener(_store: EnhancedStore): void {
    return;
  }
  
  dispose() {
    console.log('AuthFake-dispose()');
  }
}
