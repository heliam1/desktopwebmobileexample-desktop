import { Action } from '@reduxjs/toolkit';

export enum AuthActionsTypes {
  SIGNED_IN = 'SIGNED_IN',
  SIGNED_OUT = 'SIGNED_OUT',
  APP_LOADED = 'APP_LOADED'
}

export interface AuthState {
  authenticated: boolean,
  initialized: boolean,
  userId?: string
}

export interface SignedInAction extends Action {
  type: typeof AuthActionsTypes.SIGNED_IN;
  payload: string;
}

export interface SignedOutAction extends Action {
  type: typeof AuthActionsTypes.SIGNED_OUT;
}

export interface AppLoadedAction extends Action {
  type: typeof AuthActionsTypes.APP_LOADED;
}

export type AuthActions = (
  SignedInAction |
  SignedOutAction |
  AppLoadedAction
)

export function signedIn(userId: string): SignedInAction {
  return {
    type: AuthActionsTypes.SIGNED_IN,
    payload: userId
  };
}

export function signedOut(): SignedOutAction {
  return {
    type: AuthActionsTypes.SIGNED_OUT
  };
}

export function appLoaded() : AppLoadedAction {
  return {
    type: AuthActionsTypes.APP_LOADED
  };
}
