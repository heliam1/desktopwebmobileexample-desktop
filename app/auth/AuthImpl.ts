import { EnhancedStore } from '@reduxjs/toolkit';
import '@firebase/auth'
import '@firebase/functions';
import { Auth } from './Auth'
import { appLoaded, signedIn, signedOut } from './authActions';

export class AuthImpl implements Auth {
  readonly auth: firebase.auth.Auth;
  readonly functions: firebase.functions.Functions;

  constructor(dependency: firebase.app.App) {
    this.auth = dependency.auth();
    this.functions = dependency.functions();
  }

  async logInAnonymously() {
    await this.auth.signInAnonymously();
  }

  isAnon(): boolean {
    const user = this.auth.currentUser;
    if (user) {
      return user.isAnonymous;
    } else {
      return true;
    }
  }

  async deleteAnonAccount() {
    const user = this.auth.currentUser;
    if (user) {
      return user.delete();
    }
  }

  // TODO: Currently this doesn't tell the user why they can't sign up with email and password.
  signUpEmailAndPassword(email: string, password: string): Promise<void> {
    const signUp = this.functions.httpsCallable('signUpUserWithEmailAndPassword');
    return signUp({email, password}).then();
  }

  async logInEmailAndPassword(email: string, password: string) {
    try {
      await this.deleteAnonAccount();
      await this.signOut();
      await this.auth.signInWithEmailAndPassword(email, password);
    } catch (err) {
      await this.logInAnonymously();
      throw err;
    }
    
  }

  signOut(): Promise<void> {
    return this.auth.signOut();
  }

  sendPasswordResetEmail(email: string): Promise<void> {
    return this.auth.sendPasswordResetEmail(email);
  }

  userEmail(): string | null {
    const user = this.auth.currentUser;
    if (user) {
      return user.email;
    }
    return null;
  }

  userId(): string {
    const user = this.auth.currentUser;
    if (user) {
      return user!.uid;
    } else {
      return 'anon';
    }
  }

  updateUserEmail(email: string): Promise<void> {
    const user = this.auth.currentUser!;
    return user.updateEmail(email);
  }

  async deleteAccount(): Promise<void> {
    const deleteAccountCf = this.functions.httpsCallable('deleteAccount');

    const user = this.auth.currentUser!;

    deleteAccountCf({ userId: user.uid })
    .then(() => {
      return this.signOut();
    });

    return;
  }

  // TODO: consider auth epic that is utilized by root component
  activateAuthStateListener(store: EnhancedStore): void {
    this.auth.onAuthStateChanged(user => {
      if (user && !user.isAnonymous) {
        console.log("User signed in.");
        store.dispatch(signedIn(user.uid));
        store.dispatch(appLoaded());
      } else {
        console.log("User signed out.");
        // TODO: flush all memory, local db, and remote database connections
        store.dispatch(signedOut());
        store.dispatch(appLoaded());
      }
    });
  }
  
  dispose() {
    console.log('AuthImpl-dispose()');
  }
}
