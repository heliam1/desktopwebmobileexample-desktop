import { EnhancedStore } from "@reduxjs/toolkit";

export interface Auth {
  activateAuthStateListener(store: EnhancedStore): void;
  logInAnonymously(): Promise<void>;
  isAnon(): boolean;
  signUpEmailAndPassword(email: string, password: string): Promise<void>;
  logInEmailAndPassword(email: string, password: string): Promise<void>;
  signOut(): Promise<void>;
  sendPasswordResetEmail(email: string): Promise<void>;
  userId(): string;
  userEmail(): (string | null);
  updateUserEmail(email: string): Promise<void>;
  deleteAccount(): Promise<void>;
  
  dispose(): void;
}