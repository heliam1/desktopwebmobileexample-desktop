import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import { RootState } from '../store';

export interface PrivateRouteProp {
  exact: boolean,
  path: string,
  Component: any
}

export default function PrivateRoute({
  exact, path, Component
} : PrivateRouteProp ) {
  const { authenticated } = useSelector((state: RootState) => state.auth);

  if (!authenticated) {
    return <Redirect to="/"/>
  }

  return (
    <Route
      exact={exact}
      path={path}
      render={ (props) => <Component {...props} /> }
    />
  );
}