import { AuthState, AuthActions, AuthActionsTypes} from './authActions';

export const initialState: AuthState = {
  authenticated: false,
  initialized: false,
  userId: undefined
}

export function authReducer(
  state = initialState,
  action: AuthActions
) {
  switch (action.type) {
    case AuthActionsTypes.SIGNED_IN:
      return {
        ...state,
        authenticated: true,
        userId: action.payload
      }
    case AuthActionsTypes.SIGNED_OUT:
      return {
        ...state,
        authenticated: false,
        user: undefined
      }
    case AuthActionsTypes.APP_LOADED:
      return {
        ...state,
        initialized: true
      }    
    default:
      return state
  }  
}