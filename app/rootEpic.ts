import { ActionsObservable, combineEpics, StateObservable } from 'redux-observable';
import { CombinedState } from 'redux';
import { catchError } from 'rxjs/operators';
import { homeAnonLogInAndDownloadUrlEpic, homeLogInEpic, homeSignUpEpic } from './components/homeEpic'
import { eventsEpic } from './features/calendarDay/eventsEpic';
import { signOutEpic, deleteAccountEpic } from './features/settings/settingsEpic';
import { deleteEventEpic, getEventEpic, saveEventEpic } from './features/event/eventEpic';

export default function createRootEpic() {
  return (action$: ActionsObservable<any>, store$: StateObservable<CombinedState<any>>, dependencies: any) =>
  combineEpics(
    homeAnonLogInAndDownloadUrlEpic,
    homeLogInEpic,
    homeSignUpEpic,
    eventsEpic,
    signOutEpic,
    deleteAccountEpic,
    getEventEpic,
    saveEventEpic,
    deleteEventEpic
  )(action$, store$, dependencies).pipe(
    catchError((error: any, source: any) => {
      console.error(error);
      return source;
    }
  ));
};
 