/* eslint react/jsx-props-no-spreading: off */
import React from 'react';
import { RootState } from './store';
import { useSelector } from 'react-redux'
import { Switch, Route } from 'react-router-dom';
import LoadingComponent from './auth/LoadingComponent';
import PrivateRoute from './auth/PrivateRoute';
import routes from './constants/routes.json';
import App from './containers/App';
import HomePage from './containers/HomePage';

// Lazily load routes and code split with webpack
const LazySignInPage = React.lazy(() =>
  import(/* webpackChunkName: "SignInPage" */ './containers/SignInPage')
);
const LazySignUpPage = React.lazy(() =>
  import(/* webpackChunkName: "SignUpPage" */ './containers/SignUpPage')
);
const LazyCalendarDayPage = React.lazy(() =>
  import(/* webpackChunkName: "CalendarDayPage" */ './containers/CalendarDayPage')
);
const LazyCalendarWeekPage = React.lazy(() =>
  import(/* webpackChunkName: "CalendarWeekPage" */ './containers/CalendarWeekPage')
);
const LazyCalendarMonthPage = React.lazy(() =>
  import(/* webpackChunkName: "CalendarMonthPage" */ './containers/CalendarMonthPage')
);
const LazyEventPage = React.lazy(() =>
  import(/* webpackChunkName: "EventPage" */ './containers/EventPage')
);
const LazyRepeatPage = React.lazy(() =>
  import(/* webpackChunkName: "RepeatPage" */ './containers/RepeatPage')
);
const LazyConstraintPage = React.lazy(() =>
  import(/* webpackChunkName: "ConstraintPage" */ './containers/ConstraintPage')
);
const LazySettingsPage = React.lazy(() =>
  import(/* webpackChunkName: "SettingsPage" */ './containers/SettingsPage')
);
const LazyCounterPage = React.lazy(() =>
  import(/* webpackChunkName: "CounterPage" */ './containers/CounterPage')
);

const SignInPage = (props: Record<string, any>) => (
  <React.Suspense fallback={<h1>Loading...</h1>}>
    <LazySignInPage {...props} />
  </React.Suspense>
);
const SignUpPage = (props: Record<string, any>) => (
  <React.Suspense fallback={<h1>Loading...</h1>}>
    <LazySignUpPage {...props} />
  </React.Suspense>
);
const CalendarDayPage = (props: Record<string, any>) => (
  <React.Suspense fallback={<h1>Loading...</h1>}>
    <LazyCalendarDayPage {...props} />
  </React.Suspense>
);
const CalendarWeekPage = (props: Record<string, any>) => (
  <React.Suspense fallback={<h1>Loading...</h1>}>
    <LazyCalendarWeekPage {...props} />
  </React.Suspense>
);
const CalendarMonthPage = (props: Record<string, any>) => (
  <React.Suspense fallback={<h1>Loading...</h1>}>
    <LazyCalendarMonthPage {...props} />
  </React.Suspense>
);
const EventPage = (props: Record<string, any>) => (
  <React.Suspense fallback={<h1>Loading...</h1>}>
    <LazyEventPage {...props} />
  </React.Suspense>
);
const RepeatPage = (props: Record<string, any>) => (
  <React.Suspense fallback={<h1>Loading...</h1>}>
    <LazyRepeatPage {...props} />
  </React.Suspense>
);
const ConstraintPage = (props: Record<string, any>) => (
  <React.Suspense fallback={<h1>Loading...</h1>}>
    <LazyConstraintPage {...props} />
  </React.Suspense>
);
const SettingsPage = (props: Record<string, any>) => (
  <React.Suspense fallback={<h1>Loading...</h1>}>
    <LazySettingsPage {...props} />
  </React.Suspense>
);
const CounterPage = (props: Record<string, any>) => (
  <React.Suspense fallback={<h1>Loading...</h1>}>
    <LazyCounterPage {...props} />
  </React.Suspense>
);

export default function Routes() {
  const { initialized } = useSelector((state: RootState) => state.auth);

  if (!initialized) return <LoadingComponent/>

  return (
    <App>
      <Switch>
        

        <PrivateRoute exact path={routes.SETTINGS} Component={SettingsPage} />
        <PrivateRoute exact path={routes.COUNTER} Component={CounterPage} />
        <PrivateRoute exact path={routes.SIGN_IN} Component={SignInPage} />
        <PrivateRoute exact path={routes.SIGN_UP} Component={SignUpPage} />
        
        <PrivateRoute exact path={routes.CALENDAR_DAY} Component={CalendarDayPage} />
        <PrivateRoute exact path={routes.CALENDAR_WEEK} Component={CalendarWeekPage} />
        <PrivateRoute exact path={routes.CALENDAR_MONTH} Component={CalendarMonthPage} />
        <PrivateRoute exact path={routes.EVENT} Component={EventPage} />
        <PrivateRoute exact path={routes.REPEAT} Component={RepeatPage} />
        <PrivateRoute exact path={routes.CONSTRAINT} Component={ConstraintPage} />
        
        <Route exact path={routes.HOME} component={HomePage} />
      </Switch>
    </App>
  );
}
