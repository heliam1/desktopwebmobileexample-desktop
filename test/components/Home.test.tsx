import { CombinedState, configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { cleanup, fireEvent, getByLabelText, render, waitForElementToBeRemoved } from "@testing-library/react";
import React from "react";
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from "react-redux";
import { ActionsObservable, combineEpics, createEpicMiddleware, StateObservable } from "redux-observable";
import { catchError } from "rxjs/operators";
import { AuthFake } from "../../app/auth/AuthFake";
import { homeLogInEpic, homeSignUpEpic } from "../../app/components/homeEpic";
import { homeReducer } from "../../app/components/homeReducer";
import Home from './../../app/components/Home';
import { HomeState } from "../../app/components/homeActions";
import { AuthState } from "../../app/auth/authActions";
import { authReducer } from "../../app/auth/authReducer";
import { ClientLoggingFake } from "../../app/clientLogging/ClientLoggingFake";
import { StorageFake } from "../../app/storage/StorageFake";


jest.mock('./../../app/auth/Auth');

afterEach(cleanup)

function setup(
  preloadedState: {
    auth: AuthState
    home: HomeState
  } = {
    auth: {
      authenticated: false,
      initialized: true,
      userId: undefined
    },
    home: {
      loading: false,
      error: undefined,
      success: false,
      downloadUrl: '#',
    }
  }
) {
  const middleware = [...getDefaultMiddleware()];

  const dependencies = {
    auth: new AuthFake(),
    logging: new ClientLoggingFake(),
    storage: new StorageFake(),
    userAgent: 'browser-test',
  }

  const epicMiddleware = createEpicMiddleware({
    dependencies
  });

  middleware.push(epicMiddleware);

  const store = configureStore({
    reducer: { auth: authReducer, home: homeReducer },
    middleware,
    preloadedState,
  });

  epicMiddleware.run(
    (action$: ActionsObservable<any>, store$: StateObservable<CombinedState<any>>, dependencies: any) => combineEpics(
      homeSignUpEpic,
      homeLogInEpic,
    )(action$, store$, dependencies).pipe(
      catchError((err, source) => {
        console.error(err); 
        return source;
      })
    )
  );

  const getWrapper = () =>
    render(
    <Provider store={store}>
      <Router>
        <Home />
      </Router>
    </Provider>
  );

  return getWrapper();
}

describe('HomeComponent', () => {
  test('initial render', () => {
    const { queryByTestId, getByText } = setup();
    // screen.debug();
 
    // TODO: Title, logo

    expect(queryByTestId("home_loading")).toBeNull();
    expect(queryByTestId("home_error")).toBeNull();
    expect(queryByTestId("home_success")).toBeNull();
    expect(getByText("Log In"));
    expect(getByText("Sign Up"));
    expect(getByText("Download for Windows"));
    expect(getByText("Some content1"));
    expect(getByText("Some content2"));
  });

  // sign up succeeds, sign up fails, sign up fail succeed,
  test('Sign up fails then succeeds', async () => {
    const { findByTestId, queryByTestId, getByLabelText, getByText } = setup();
    // screen.debug();

    const mockSignUpWithEmailAndPassword = jest.fn();
    AuthFake.prototype.signUpEmailAndPassword = mockSignUpWithEmailAndPassword;
    mockSignUpWithEmailAndPassword.mockRejectedValueOnce(new Error('Failed.'));

    const emailInput = queryByTestId("home_email")!;
    const passwordInput = queryByTestId("home_password")!;
    fireEvent.change(emailInput, { target: { value: "example@example.com" } });
    fireEvent.change(passwordInput, { target: { value: "Example123!" } });
    fireEvent.click(getByText('Sign Up'));

    expect(queryByTestId("home_error")).toBeNull();
    queryByTestId("home_loading");

    const errorUi = await findByTestId('home_error');
    expect(queryByTestId("home_loading")).toBeNull();

    mockSignUpWithEmailAndPassword.mockImplementationOnce(() => Promise.resolve());

    fireEvent.click(getByText('Sign Up'));
    expect(queryByTestId("home_error")).toBeNull();
    queryByTestId("home_loading");

    await waitForElementToBeRemoved(() => queryByTestId("home_loading"))
    expect(queryByTestId('home_error')).toBeNull();
  });
  
  // log in succeeds, log in fails, log in fail suceed
  test('Log in fails then succeeds', async () => {
    const { findByTestId, queryByTestId, getByLabelText, getByText } = setup();
    // screen.debug();

    const mockLogInWithEmailAndPassword = jest.fn();
    AuthFake.prototype.logInEmailAndPassword = mockLogInWithEmailAndPassword;
    mockLogInWithEmailAndPassword.mockRejectedValueOnce(new Error('Failed.'))

    const emailInput = queryByTestId("home_email")!;
    const passwordInput = queryByTestId("home_password")!;
    fireEvent.change(emailInput, { target: { value: "example@example.com" } });
    fireEvent.change(passwordInput, { target: { value: "Example123!" } });
    fireEvent.click(getByText('Log In'));

    expect(queryByTestId("home_error")).toBeNull();
    queryByTestId("home_loading");

    const errorUi = await findByTestId('home_error')!;
    expect(queryByTestId("home_loading")).toBeNull();

    mockLogInWithEmailAndPassword.mockImplementationOnce(() => Promise.resolve());

    fireEvent.click(getByText('Log In'));
    expect(queryByTestId("home_error")).toBeNull();
    queryByTestId("home_loading");

    await waitForElementToBeRemoved(() => queryByTestId("home_loading"))
    expect(queryByTestId('home_error')).toBeNull();
  });

  /* TODO probs do with puppeteer.
  test('Sign up succeeds and navigates to calendar day', () => {
    render(<Home />);
 
    screen.debug();
 
    expect(screen.getByText(/Searches for JavaScript/)).toBeNull();
  });
  */

});
