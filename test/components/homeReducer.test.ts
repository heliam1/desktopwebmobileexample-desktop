import { anonLogInAndDownloadUrl, anonLogInAndDownloadUrlSuccess, error, HomeState, logIn, logInSuccess, signUp, signUpSuccess } from '../../app/components/homeActions';
import { homeReducer, initialState } from '../../app/components/homeReducer';

describe('Home Reducer', () => {
  test('initial state', () => {
    const action: any = {};

    expect(homeReducer(undefined, action)).toEqual(initialState)
  });

  test('sign up', () => {
    const currentState: HomeState = {
      loading: false,
      error: undefined,
      success: false,
      downloadUrl: 'https://fakeUrl.com',
    };
    const action = signUp('example@example.com', 'example123!');
    const expectedNextState: HomeState = {
      loading: true,
      error: undefined,
      success: false,
      downloadUrl: 'https://fakeUrl.com',
    }

    const actualNextState: HomeState = homeReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  test('sign up success', () => {
    const currentState: HomeState = {
      loading: true,
      error: undefined,
      success: false,
      downloadUrl: 'https://fakeUrl.com',
    };
    const action = signUpSuccess();
    const expectedNextState: HomeState = {
      loading: false,
      error: undefined,
      success: true,
      downloadUrl: 'https://fakeUrl.com',
    }

    const actualNextState: HomeState = homeReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  test('log in after successful sign up', () => {
    const currentState: HomeState = {
      loading: false,
      error: undefined,
      success: true,
      downloadUrl: 'https://fakeUrl.com',
    };
    const action = logIn('example@example.com', 'example123!');
    const expectedNextState: HomeState = {
      loading: true,
      error: undefined,
      success: false,
      downloadUrl: 'https://fakeUrl.com',
    }

    const actualNextState: HomeState = homeReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  test('log in after error logging in, and successful sign up', () => {
    const currentState: HomeState = {
      loading: false,
      error: 'Failed.',
      success: true,
      downloadUrl: 'https://fakeUrl.com',
    };
    const action = logIn('example@example.com', 'example123!');
    const expectedNextState: HomeState = {
      loading: true,
      error: undefined,
      success: false,
      downloadUrl: 'https://fakeUrl.com',
    }

    const actualNextState: HomeState = homeReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  test('log in success', () => {
    const currentState: HomeState = {
      loading: true,
      error: undefined,
      success: true,
      downloadUrl: 'https://fakeUrl.com',
    };
    const action = logInSuccess();
    const expectedNextState: HomeState = {
      loading: false,
      error: undefined,
      success: false,
      downloadUrl: 'https://fakeUrl.com',
    }

    const actualNextState: HomeState = homeReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  test('error', () => {
    const currentState: HomeState = {
      loading: true,
      error: undefined,
      success: false,
      downloadUrl: 'https://fakeUrl.com',
    };
    const action = error('Failed.');
    const expectedNextState: HomeState = {
      loading: false,
      error: 'Failed.',
      success: false,
      downloadUrl: 'https://fakeUrl.com',
    }

    const actualNextState: HomeState = homeReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  test('error after success', () => {
    const currentState: HomeState = {
      loading: true,
      error: undefined,
      success: true,
      downloadUrl: 'https://fakeUrl.com',
    };
    const action = error('Failed.');
    const expectedNextState: HomeState = {
      loading: false,
      error: 'Failed.',
      success: false,
      downloadUrl: 'https://fakeUrl.com',
    }

    const actualNextState: HomeState = homeReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  test('anon log in and download url does not change any state', () => {
    const currentState: HomeState = {
      loading: false,
      error: undefined,
      success: false,
      downloadUrl: '#',
    };
    const action = anonLogInAndDownloadUrl();
    const expectedNextState: HomeState = {
      loading: false,
      error: undefined,
      success: false,
      downloadUrl: '#',
    }

    const actualNextState: HomeState = homeReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  test('anon log in and download url success updates the url', () => {
    const currentState: HomeState = {
      loading: false,
      error: undefined,
      success: false,
      downloadUrl: '#',
    };
    const action = anonLogInAndDownloadUrlSuccess('https://fakeUrl.com');
    const expectedNextState: HomeState = {
      loading: false,
      error: undefined,
      success: false,
      downloadUrl: 'https://fakeUrl.com',
    }

    const actualNextState: HomeState = homeReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

});
