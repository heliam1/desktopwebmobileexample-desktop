import { ActionsObservable, StateObservable } from 'redux-observable';
import { of, Subject } from 'rxjs';
import { toArray } from 'rxjs/operators';
import { anonLogInAndDownloadUrl, anonLogInAndDownloadUrlSuccess, error, HomeState, logIn, logInSuccess, signUp, signUpSuccess } from './../../app/components/homeActions';
import { homeAnonLogInAndDownloadUrlEpic, homeSignUpEpic } from './../../app/components/homeEpic';
import { homeLogInEpic } from './../../app/components/homeEpic';
import { AuthFake } from './../../app/auth/AuthFake';
import { initialState } from './../../app/components/homeReducer';
import { RootState } from './../../app/store';
import { ClientLoggingFake } from "../../app/clientLogging/ClientLoggingFake";
import { StorageFake } from '../../app/storage/StorageFake';

describe('homeEpic', () => {

  let state: { home: HomeState };

  beforeEach(() => {
    state = { home: initialState};
  })

  // SIGN UP  
  
  test('Send success action on Auth sign up success.', async () => {
    const dependencies = {
      auth: new AuthFake(),
      logging: new ClientLoggingFake(),
      userAgent: 'browser-test',
    }

    const action$ = ActionsObservable.of(
      signUp('example@example.com', 'Example123!')
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = homeSignUpEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      signUpSuccess()
    ]);
  });

  test('Send error action on Auth sign up failure.', async () => {
    jest.mock('./../../app/auth/Auth');

    const mockSignUpEmailAndPassword = jest.fn();
    AuthFake.prototype.signUpEmailAndPassword = mockSignUpEmailAndPassword;
    
    mockSignUpEmailAndPassword
    .mockRejectedValueOnce(new Error('Failed.'));
    
    const dependencies = {
      auth: new AuthFake(),
      logging: new ClientLoggingFake(),
      userAgent: 'browser-test',
    }

    const action$ = ActionsObservable.of(
      signUp('example@example.com', 'Example123!')
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = homeSignUpEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      error('Account may already exist.'),
    ]);
  });

  test('Sign up fail does not affect future sign up success.', async () => {
    jest.mock('./../../app/auth/Auth');
    const mockSignUpEmailAndPassword = jest.fn();
    AuthFake.prototype.signUpEmailAndPassword = mockSignUpEmailAndPassword;
    
    mockSignUpEmailAndPassword
    .mockRejectedValueOnce(new Error('Failed.'))
    .mockImplementationOnce(() => Promise.resolve());

    const dependencies = {
      auth: new AuthFake(),
      logging: new ClientLoggingFake(),
      userAgent: 'browser-test',
    }

    const action$ = ActionsObservable.of(
      signUp('example@example.com', 'Example123!'),
      signUp('example@example.com', 'Example123!'),
    );

    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = homeSignUpEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      error('Account may already exist.'),
      signUpSuccess(),
    ]);
  });

  // LOG IN

  test('Send success action on Auth log in success.', async () => {
    const dependencies = {
      auth: new AuthFake(),
      logging: new ClientLoggingFake(),
      userAgent: 'browser-test',
    }

    const action$ = ActionsObservable.of(
      logIn('example@example.com', 'Example123!')
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = homeLogInEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      logInSuccess()
    ]);
  });

  test('Send error action on Auth log in failure.', async () => {
    jest.mock('./../../app/auth/Auth');

    const mockLogInEmailAndPassword = jest.fn();
    AuthFake.prototype.logInEmailAndPassword = mockLogInEmailAndPassword;
    
    mockLogInEmailAndPassword
    .mockRejectedValueOnce(new Error('Failed.'));
    
    const dependencies = {
      auth: new AuthFake(),
      logging: new ClientLoggingFake(),
      userAgent: 'browser-test',
    }

    const action$ = ActionsObservable.of(
      logIn('example@example.com', 'Example123!')
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = homeLogInEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      error('Failed.'),
    ]);
  });

  test('Log in fail does not affect future sign up success.', async () => {
    jest.mock('./../../app/auth/Auth');
    const mockLogInEmailAndPassword = jest.fn();
    AuthFake.prototype.logInEmailAndPassword = mockLogInEmailAndPassword;
    
    mockLogInEmailAndPassword
    .mockRejectedValueOnce(new Error('Failed.'))
    .mockImplementationOnce(() => Promise.resolve());

    const dependencies = {
      auth: new AuthFake(),
      logging: new ClientLoggingFake(),
      userAgent: 'browser-test',
    }

    const action$ = ActionsObservable.of(
      logIn('example@example.com', 'Example123!'),
      logIn('example@example.com', 'Example123!'),
    );

    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = homeLogInEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      error('Failed.'),
      logInSuccess(),
    ]);
  });

  // ANON LOG IN AND DOWNLOAD URL
  
  // succeed doesn't do anything
  test('Send success action on anonLogInAndDownloadUrl success.', async () => {
    const dependencies = {
      auth: new AuthFake(),
      logging: new ClientLoggingFake(),
      storage: new StorageFake(),
      userAgent: 'browser-test',
    }

    const action$ = ActionsObservable.of(
      anonLogInAndDownloadUrl(),
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = homeAnonLogInAndDownloadUrlEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      anonLogInAndDownloadUrlSuccess('https://fakeUrl.com'),
    ]);
  });
  
  // error creates error
  test('Send error action on anonLogInAndDownloadUrl success.', async () => {
    jest.mock('./../../app/storage/StorageFake');
    const mockGetDownloadUrls = jest.fn();
    StorageFake.prototype.getDownloadUrls = mockGetDownloadUrls;
    
    mockGetDownloadUrls
    .mockRejectedValueOnce(new Error('Failed.'))

    const dependencies = {
      auth: new AuthFake(),
      logging: new ClientLoggingFake(),
      storage: new StorageFake(),
      userAgent: 'browser-test',
    }

    const action$ = ActionsObservable.of(
      anonLogInAndDownloadUrl(),
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = homeAnonLogInAndDownloadUrlEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      error('Failed.'),
    ]);
  });
});