import { ClientLoggingImpl } from "../../app/clientLogging/ClientLoggingImpl";
import { ClientLoggingDbFake } from "../../app/clientLogging/clientLoggingDb/ClientLoggingDbFake";

describe('Client Logging', () => {
  
  beforeAll(async () => {
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  

  function wait(milliseconds: number) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
  }

  test('Timeout functionality works as expected', async () => {

    jest.mock('../../app/clientLogging/clientLoggingDb/ClientLoggingDbFake');
    const mockClientLoggingDbFakeWriteLog = jest.fn();
    ClientLoggingDbFake.prototype.writeLog = mockClientLoggingDbFakeWriteLog;

    const clientLogging = new ClientLoggingImpl(new ClientLoggingDbFake());

    const date1 = new Date();

    clientLogging.startTrace(
      'trace1',
      date1,
      'liam',
      false,
      'chrome',
      1000
    );

    await wait(2000);

    expect(mockClientLoggingDbFakeWriteLog.mock.calls.length).toBe(1);
  });
  
  // start trace end trace work as expected
  test('Start trace and end trace works as expected', async () => {

    jest.mock('../../app/clientLogging/clientLoggingDb/ClientLoggingDbFake');
    const mockClientLoggingDbFakeWriteLog = jest.fn();
    ClientLoggingDbFake.prototype.writeLog = mockClientLoggingDbFakeWriteLog;

    const clientLogging = new ClientLoggingImpl(new ClientLoggingDbFake());

    const date1 = new Date();

    clientLogging.startTrace(
      'trace1',
      date1,
      'liam',
      false,
      'chrome',
      60
    );
    
    const date2 = new Date();

    clientLogging.startTrace(
      'trace2',
      date2,
      'liam',
      false,
      'chrome',
      60
    );

    const result1 = clientLogging.endTrace(
      'trace1',
      'liam',
      false,
    );

    const result2 = clientLogging.endTrace(
      'trace2',
      'liam',
      false,
    );

    expect(result1 && result2);
    // The mock function is called twice
    expect(mockClientLoggingDbFakeWriteLog.mock.calls.length).toBe(2);
    // The first argument of the first call to the function was 0
    // expect(mockCallback.mock.calls[0][0]).toBe(0);
    // The first argument of the second call to the function was 1
    // expect(mockCallback.mock.calls[1][0]).toBe(1);
    // The return value of the first call to the function was 42
    // expect(mockCallback.mock.results[0].value).toBe(42);
  });
  
  // what if a trace tries to be ended when it was already errord? Do nothing.
  test('Cannot double end a trace', async () => {

    jest.mock('../../app/clientLogging/clientLoggingDb/ClientLoggingDbFake');
    const mockClientLoggingDbFakeWriteLog = jest.fn();
    ClientLoggingDbFake.prototype.writeLog = mockClientLoggingDbFakeWriteLog;

    const clientLogging = new ClientLoggingImpl(new ClientLoggingDbFake());

    const date1 = new Date();

    clientLogging.startTrace(
      'trace1',
      date1,
      'liam',
      false,
      'chrome',
      60
    );

    const result1 = clientLogging.endTrace(
      'trace1',
      'liam',
      false,
    );

    const result2 = clientLogging.endTrace(
      'trace1',
      'liam',
      false,
    );

    expect(result1).toEqual(true);
    expect(result2).toEqual(false);
    expect(mockClientLoggingDbFakeWriteLog.mock.calls.length).toBe(1);
  });

  // @visible for testing endtrace is gone after timeout, or just endtrace returns bool so we can test.

});