import { deleteAccount, error, signOut, success } from "../../../app/features/settings/settingsActions";
import { settingsReducer, initialState } from "../../../app/features/settings/settingsReducer";

/*
create the beforeState
afterState = reducer(beforeState, someAction)
assert after state == expectedAfterState
*/

describe('Settings Reducer', () => {
  test('initial state', () => {
    const action: any = {};

    expect(settingsReducer(undefined, action)).toEqual(initialState)
  });

  test('signOut', () => {
    expect(
      settingsReducer(
        initialState,
        signOut()
      )
    ).toEqual(
      {
        loading: true,
        error: undefined
      }
    );
  });

  test('deleteAccount', () => {
    expect(
      settingsReducer(
        initialState,
        deleteAccount()
      )
    ).toEqual(
      {
        loading: true,
        error: undefined
      }
    );
  });

  test('error', () => {
    expect(
      settingsReducer(
        {
          loading: true,
          error: undefined
        },
        error('Failed.'),
      )
    ).toEqual(
      {
        loading: false,
        error: 'Failed.',
      }
    );
  });

  test('success', () => {
    const currentState = {
      loading: true,
      error: undefined
    };
    const action = success();
    const expectedNextState = {
      loading: false,
      error: undefined,
    }

    const actualNextState = settingsReducer(
      currentState,
      action,
    );

    expect(actualNextState).toEqual(expectedNextState);
  });
});
