import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { CombinedState, configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';

import Settings from '../../../app/features/settings/Settings';
import { SettingsState } from '../../../app/features/settings/settingsActions';
import { settingsReducer } from '../../../app/features/settings/settingsReducer';
import { AuthFake } from '../../../app/auth/AuthFake'
import { ActionsObservable, combineEpics, createEpicMiddleware, StateObservable } from 'redux-observable';
import { signOutEpic, deleteAccountEpic } from '../../../app/features/settings/settingsEpic';
import { catchError } from 'rxjs/operators';
import { waitForElementToBeRemoved } from '@testing-library/react';

// use automatic mocks if don't need to replace impl
// use 

jest.mock('../../../app/auth/Auth');

afterEach(cleanup)

function setup(
  preloadedState: {
    settings: SettingsState
  } = { settings: { loading: false, error: undefined } }
) {
  const middleware = [...getDefaultMiddleware()];

  const dependencies = {
    auth: new AuthFake(),
  }

  const epicMiddleware = createEpicMiddleware({
    dependencies
  });

  middleware.push(epicMiddleware);

  const store = configureStore({
    reducer: { settings: settingsReducer },
    middleware,
    preloadedState,
  });

  epicMiddleware.run(
    (action$: ActionsObservable<any>, store$: StateObservable<CombinedState<any>>, dependencies: any) => combineEpics(
      signOutEpic,
      deleteAccountEpic,
    )(action$, store$, dependencies).pipe(
      catchError((error, source) => {
        console.error(error);
        return source;
      })
    )
  );

  const getWrapper = () =>
    render(
    <Provider store={store}>
      <Router>
        <Settings />
      </Router>
    </Provider>
  );

  return getWrapper();
}

describe('SettingsComponent', () => {
  test('initial render', () => {
    const { queryByTestId, getByText } = setup();
    // screen.debug();
 
    expect(queryByTestId("settings_loading")).toBeNull();
    expect(queryByTestId("settings_error")).toBeNull();
    expect(getByText("Sign Out"));
  });


  test('Sign out fails then succeeds', async () => { // test sign out fails then succeeds.
    const { findByTestId, queryByTestId, getByText } = setup();
    // screen.debug();

    // https://dev.to/jackcaldwell/mocking-es6-class-methods-with-jest-bd7
    const mockSignOut = jest.fn();
    AuthFake.prototype.signOut= mockSignOut;
    mockSignOut.mockRejectedValueOnce(new Error('Failed.'))

    expect(queryByTestId("settings_error")).toBeNull();

    fireEvent.click(getByText('Sign Out'));

    queryByTestId("settings_loading");
    const errorUi = await findByTestId('settings_error');
    expect(queryByTestId("settings_loading")).toBeNull();

    mockSignOut.mockImplementationOnce(() => Promise.resolve());

    fireEvent.click(getByText('Sign Out'));
    expect(queryByTestId("settings_error")).toBeNull();
    queryByTestId("settings_loading");

    await waitForElementToBeRemoved(() => queryByTestId("settings_loading"))
    expect(queryByTestId('settings_error')).toBeNull();
  });

  test('Delete account fails then succeeds', async () => { // test sign out fails then succeeds.
    const { findByTestId, queryByTestId, getByText } = setup();
    // screen.debug();

    const mockDeleteAccount = jest.fn();
    AuthFake.prototype.deleteAccount = mockDeleteAccount;
    mockDeleteAccount.mockRejectedValueOnce(new Error('Failed.'))

    expect(queryByTestId("settings_error")).toBeNull();

    fireEvent.click(getByText('Delete Account'));

    queryByTestId("settings_loading");
    const errorUi = await findByTestId('settings_error');
    expect(queryByTestId("settings_loading")).toBeNull();

    mockDeleteAccount.mockImplementationOnce(() => Promise.resolve());

    fireEvent.click(getByText('Delete Account'));
    expect(queryByTestId("settings_error")).toBeNull();
    queryByTestId("settings_loading");

    await waitForElementToBeRemoved(() => queryByTestId("settings_loading"))
    expect(queryByTestId('settings_error')).toBeNull();
  });

});

