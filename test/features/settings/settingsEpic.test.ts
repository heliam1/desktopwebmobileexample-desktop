import { ActionsObservable, StateObservable } from 'redux-observable';
import { Subject } from 'rxjs';
import { toArray } from 'rxjs/operators';
import { signOut, error, success, SettingsState, deleteAccount } from './../../../app/features/settings/settingsActions';
import { signOutEpic, deleteAccountEpic } from './../../../app/features/settings/settingsEpic';
import { AuthFake } from './../../../app/auth/AuthFake';
import { initialState } from '../../../app/features/settings/settingsReducer';
import { RootState } from '../../../app/store';

// https://medium.com/kevin-salters-blog/writing-epic-unit-tests-bd85f05685b
// https://codeburst.io/unit-testing-redux-observable-based-epics-using-sinon-and-jest-c2003c2dc6fa
/*
So let us focus on the first test for now. We need pass the epic the dispatching action and get the resulting action when the RxJS Observable completes. There are many ways to write such code, but the following works the best for me;
*/
// https://itnext.io/going-epic-with-redux-observable-tests-dd42b80ee4f8


// https://levelup.gitconnected.com/extensive-introduction-to-why-and-how-you-might-want-to-use-and-test-redux-observable-1f2987407166

describe('settingsEpic', () => {

  let state: { settings: SettingsState };

  beforeEach(() => {
    state = { settings: initialState };
  })
  
  test('send success action on Auth success', async () => {

    // optional mocking

    const dependencies = {
      auth: new AuthFake(),
    }

    const action$ = ActionsObservable.of(
      signOut()
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = signOutEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      success()
    ]);
  });

  test('send fail action on Auth fail', async () => {
    jest.mock('../../../app/auth/Auth');

    const mockSignOut = jest.fn();
    AuthFake.prototype.signOut= mockSignOut;
    
    mockSignOut
    .mockRejectedValueOnce(new Error('Failed.'));

    const dependencies = {
      auth: new AuthFake(),
    }

    const action$ = ActionsObservable.of(
      signOut()
    );

    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = signOutEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      error('Failed.')
    ]);
  });

  test('Auth Fail does not affect future Auth Success', async () => {
    jest.mock('../../../app/auth/Auth');
    const mockSignOut = jest.fn();
    AuthFake.prototype.signOut= mockSignOut;
    
    mockSignOut
    .mockRejectedValueOnce(new Error('Failed.'))
    .mockImplementationOnce(() => Promise.resolve());

    const dependencies = {
      auth: new AuthFake(),
    }

    const action$ = ActionsObservable.of(
      signOut(),
      signOut(),
    );

    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = signOutEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      error('Failed.'),
      success(),
    ]);
  });

  test('send success action on delete account success', async () => {

    // optional mocking

    const dependencies = {
      auth: new AuthFake(),
    }

    const action$ = ActionsObservable.of(
      deleteAccount()
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = deleteAccountEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      success()
    ]);
  });

  test('send fail action on delete account fail', async () => {
    jest.mock('../../../app/auth/Auth');

    const mockDeleteAccount = jest.fn();
    AuthFake.prototype.deleteAccount = mockDeleteAccount;
    
    mockDeleteAccount
    .mockRejectedValueOnce(new Error('Failed.'));

    const dependencies = {
      auth: new AuthFake(),
    }

    const action$ = ActionsObservable.of(
      deleteAccount()
    );

    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = deleteAccountEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      error('Failed.')
    ]);
  });

  test('Delete Account Fail does not affect future Delete Account Success', async () => {
    jest.mock('../../../app/auth/Auth');
    const mockDeleteAccount = jest.fn();
    AuthFake.prototype.deleteAccount = mockDeleteAccount;
    
    mockDeleteAccount
    .mockRejectedValueOnce(new Error('Failed.'))
    .mockImplementationOnce(() => Promise.resolve());

    const dependencies = {
      auth: new AuthFake(),
    }

    const action$ = ActionsObservable.of(
      deleteAccount(),
      deleteAccount(),
    );

    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = deleteAccountEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      error('Failed.'),
      success(),
    ]);
  });
});
