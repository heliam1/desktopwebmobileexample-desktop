import { errorEvents, EventsState, listenEvents, receiveEvents } from "../../../app/features/calendarDay/eventsActions";
import { eventsReducer, initialState } from "../../../app/features/calendarDay/eventsReducer";

describe('Calendar Day Reducer', () => {
  
  test('initial state', () => {
    const action: any = {};

    expect(eventsReducer(undefined, action)).toEqual(initialState)
  });

  test('Listen events', () => {
    const currentState: EventsState = {
      loading: false,
      events: [],
      error: undefined,
    };
    const action = listenEvents('fakeUserId', 71735741110);
    const expectedNextState: EventsState = {
      loading: true,
      events: [],
      error: undefined,
    }

    const actualNextState = eventsReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  test('Receive events', () => {
    const currentState: EventsState = {
      loading: true,
      events: [],
      error: undefined,
    };
    const action = receiveEvents([
      { id: 'eventId1', name: 'eventName1' },
      { id: 'eventId2', name: 'eventName2' }
    ]);
    const expectedNextState: EventsState = {
      loading: false,
      events: [
        { id: 'eventId1', name: 'eventName1' },
        { id: 'eventId2', name: 'eventName2' }
      ],
      error: undefined,
    }

    const actualNextState = eventsReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  test('Error events', () => {
    const currentState: EventsState = {
      loading: true,
      events: [],
      error: undefined,
    };
    const action = errorEvents('Failed.');
    const expectedNextState: EventsState = {
      loading: false,
      events: [],
      error: 'Failed.',
    }

    const actualNextState = eventsReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

});
