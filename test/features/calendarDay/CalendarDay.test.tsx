import { CombinedState, configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { cleanup, fireEvent, render, waitForElementToBeRemoved } from "@testing-library/react";
import { createMemoryHistory } from "history";
import React from "react";
import { Provider } from "react-redux";
import { ActionsObservable, combineEpics, createEpicMiddleware, StateObservable } from "redux-observable";
import { catchError } from "rxjs/operators";
import { AuthFake } from "../../../app/auth/AuthFake";
import { EventsState } from "../../../app/features/calendarDay/eventsActions";
import { eventsEpic } from "../../../app/features/calendarDay/eventsEpic";
import { eventsReducer } from "../../../app/features/calendarDay/eventsReducer";
import { RemoteDbFake } from "../../../app/remoteDb/RemoteDbFake";
import { Route, Router } from "react-router-dom";
import CalendarDay from "../../../app/features/calendarDay/CalendarDay";
import { from } from "rxjs";
import { authReducer } from "../../../app/auth/authReducer";
import { AuthState } from "../../../app/auth/authActions";

afterEach(cleanup)

function setup(
  ui,
  {
    route = '/',
    history = createMemoryHistory({
      initialEntries: [route]
    }),
  } = {},
  preloadedState: {
    auth: AuthState,
    events: EventsState
  } = {
    auth: {
      authenticated: true,
      initialized: true,
      userId: 'A valid user id',
    },
    events: {
      loading: true,
      events: [],
      error: undefined
    }
  },
) {
  const middleware = [...getDefaultMiddleware()];

  const dependencies = {
    nav: history,
    auth: new AuthFake(),
    remoteDb: new RemoteDbFake(),
  }

  const epicMiddleware = createEpicMiddleware({
    dependencies
  });

  middleware.push(epicMiddleware);

  const store = configureStore({
    reducer: { auth: authReducer, events: eventsReducer },
    middleware,
    preloadedState,
  });

  epicMiddleware.run(
    (action$: ActionsObservable<any>, store$: StateObservable<CombinedState<any>>, dependencies: any) => combineEpics(
      eventsEpic
    )(action$, store$, dependencies).pipe(
      catchError((err, source) => {
        console.error(err);
        return source;
      })
    )
  );

  return {
    ...render(
      <Provider store={store}>
        <Router history={history}>
          {ui}
        </Router>
      </Provider>
    )
  }
}

describe('CalendarDayComponent', () => {
  // https://github.com/bvaughn/react-virtualized/issues/493
  const originalOffsetHeight = Object.getOwnPropertyDescriptor(HTMLElement.prototype, 'offsetHeight');
  const originalOffsetWidth = Object.getOwnPropertyDescriptor(HTMLElement.prototype, 'offsetWidth');

  beforeAll(() => {
    Object.defineProperty(HTMLElement.prototype, 'offsetHeight', { configurable: true, value: 50 });
    Object.defineProperty(HTMLElement.prototype, 'offsetWidth', { configurable: true, value: 50 });
  });

  afterAll(() => {
    Object.defineProperty(HTMLElement.prototype, 'offsetHeight', originalOffsetHeight);
    Object.defineProperty(HTMLElement.prototype, 'offsetWidth', originalOffsetWidth);
  });

  test('initial render success listen events', async () => {
    
    jest.mock('../../../app/remoteDb/RemoteDbFake');
    const mockListenEvents = jest.fn();
    RemoteDbFake.prototype.listenEvents = mockListenEvents;
    mockListenEvents
    .mockImplementationOnce(() => from(
      new Promise(
        resolve => setTimeout(() => resolve([
          {
            id: "1",
            name: "name1",
          },
          {
            id: "2",
            name: "name2",
          }
        ]),
        250)
      )
    ));

    const { queryByTestId, getByText } = setup(
      <Route path='/calendar/day'>
        <CalendarDay/>
      </Route>,
      {
        route: '/calendar/day',
      }
    );

    expect(queryByTestId("calendar_day_loading"));
    expect(queryByTestId("calendar_day_error")).toBeNull();
    expect(queryByTestId("calendar_day_fab"));

    await waitForElementToBeRemoved(() => queryByTestId("calendar_day_loading"));

    expect(getByText("name1"));
    expect(getByText("name2"));
    expect(queryByTestId("calendar_day_error")).toBeNull();
  });

  test('initial render fail listen events', async () => {
    
    jest.mock('../../../app/remoteDb/RemoteDbFake');
    const mockListenEvents = jest.fn();
    RemoteDbFake.prototype.listenEvents = mockListenEvents;
    mockListenEvents
    .mockImplementationOnce(() => from(Promise.reject(new Error('Failed.'))));

    const { queryByTestId, getByText } = setup(
      <Route path='/calendar/day'>
        <CalendarDay/>
      </Route>,
      {
        route: '/calendar/day',
      }
    );

    expect(queryByTestId("calendar_day_loading"));
    expect(queryByTestId("calendar_day_error")).toBeNull();
    expect(queryByTestId("calendar_day_fab"));

    await waitForElementToBeRemoved(() => queryByTestId("calendar_day_loading"));

    expect(queryByTestId("calendar_day_error"));
  });

  test('fab navigates to new event', async () => {
    
    jest.mock('../../../app/remoteDb/RemoteDbFake');
    const mockListenEvents = jest.fn();
    RemoteDbFake.prototype.listenEvents = mockListenEvents;
    mockListenEvents
    .mockImplementationOnce(() => from(
      new Promise(
        resolve => setTimeout(() => resolve([
          {
            id: "1",
            name: "name1",
          },
          {
            id: "2",
            name: "name2",
          }
        ]),
        250)
      )
    ));

    const { queryByTestId, getByText } = setup(
      <Route path='/calendar/day'>
        <CalendarDay/>
      </Route>,
      {
        route: '/calendar/day',
      }
    );

    await waitForElementToBeRemoved(() => queryByTestId("calendar_day_loading"));

    fireEvent.click(queryByTestId("calendar_day_fab"));

    expect(queryByTestId("event_loading"));
  });


  test('list item navigates to event', async () => {
    
    jest.mock('../../../app/remoteDb/RemoteDbFake');
    const mockListenEvents = jest.fn();
    RemoteDbFake.prototype.listenEvents = mockListenEvents;
    mockListenEvents
    .mockImplementationOnce(() => from(
      new Promise(
        resolve => setTimeout(() => resolve([
          {
            id: "1",
            name: "name1",
          },
          {
            id: "2",
            name: "name2",
          }
        ]),
        250)
      )
    ));

    const { queryByTestId, getByText } = setup(
      <Route path='/calendar/day'>
        <CalendarDay/>
      </Route>,
      {
        route: '/calendar/day',
      }
    );

    await waitForElementToBeRemoved(() => queryByTestId("calendar_day_loading"));

    fireEvent.click(getByText("name1"));
    expect(queryByTestId("event_loading"));
  });

  // TODO: test scroll bar is appropriately sized?
});
