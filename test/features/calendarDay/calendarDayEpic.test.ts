import { createHashHistory } from "history";
import { ActionsObservable, StateObservable } from "redux-observable";
import { from, Subject } from "rxjs";
import { toArray } from "rxjs/operators";
import { AuthFake } from "../../../app/auth/AuthFake";
import { errorEvents, EventsState, listenEvents, receiveEvents } from "../../../app/features/calendarDay/eventsActions";
import { eventsEpic } from "../../../app/features/calendarDay/eventsEpic";
import { RemoteDbFake } from "../../../app/remoteDb/RemoteDbFake";
import { Dependencies, RootState } from "../../../app/store";

describe('Calendar Day Epic.', () => {

  let state: { events: EventsState };

  beforeEach(() => {
    state = {
      events: {
        loading: true,
        events: [],
        error: undefined,
      },
    };
  })

  test('Send success/receive action on listen Events success.', async () => {

    const dependencies: Dependencies = {
      nav: createHashHistory(),
      auth: new AuthFake(),
      remoteDb: new RemoteDbFake(),
    }

    const action$ = ActionsObservable.of(
      listenEvents('fakeUserId', 71735741110)
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = eventsEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      receiveEvents([
        {
          id: "1",
          name: "name1",
        },
        {
          id: "2",
          name: "name2",
        }
      ])
    ]);
  });

  test('Send error action on listen Events error.', async () => {

    jest.mock('../../../app/remoteDb/RemoteDbFake');
    const mockListenEvents = jest.fn();
    RemoteDbFake.prototype.listenEvents = mockListenEvents;
    
    mockListenEvents
    .mockImplementationOnce(() => from(Promise.reject(new Error('Failed.'))));

    const dependencies: Dependencies = {
      nav: createHashHistory(),
      auth: new AuthFake(),
      remoteDb: new RemoteDbFake(),
    }

    const action$ = ActionsObservable.of(
      listenEvents('fakeUserId', 71735741110)
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = eventsEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      errorEvents('Failed.')
    ]);
  });

});
