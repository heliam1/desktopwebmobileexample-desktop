import { ActionsObservable, StateObservable } from "redux-observable";
import { Subject } from "rxjs";
import { toArray } from "rxjs/operators";
import { deleteError, deleteEvent, deleteSuccess, EventState, getError, getEvent, getSuccess, saveError, saveEvent, saveSuccess } from "../../../app/features/event/eventActions";
import { deleteEventEpic, getEventEpic, saveEventEpic } from "../../../app/features/event/eventEpic";
import { Dependencies, RootState } from "../../../app/store";
import { initialState } from "../../../app/features/calendarDay/eventsReducer";
import { RemoteDbFake } from "../../../app/remoteDb/RemoteDbFake";
import { AuthFake } from "../../../app/auth/AuthFake";
import { createHashHistory } from "history";
import { AuthState } from "../../../app/auth/authActions";

describe('Event epic.', () => {

  let state: { event: EventState, auth: AuthState};

  beforeEach(() => {
    state = {
      event: {
        loading: false,
        event: { id: "loading", name: "loading" },
        error: undefined
      },
      auth: {
        authenticated: true,
        initialized: true,
        userId: 'A valid user id',
      }
    };
  })

  test('Send success action on save Event success.', async () => {

    // optional mocking

    const dependencies: Dependencies = {
      nav: createHashHistory(),
      auth: new AuthFake(),
      remoteDb: new RemoteDbFake(),
    }

    const action$ = ActionsObservable.of(
      saveEvent({ id: 'newEventKey', name: 'eventName'})
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = saveEventEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      saveSuccess()
    ]);
  });

  test('Send fail action on save Event fail.', async () => {
    jest.mock('../../../app/remoteDb/RemoteDbFake');
    const mockSaveEvent = jest.fn();
    RemoteDbFake.prototype.saveEvent = mockSaveEvent;
    
    mockSaveEvent
    .mockRejectedValueOnce(new Error('Failed.'));

    const dependencies: Dependencies = {
      nav: createHashHistory(),
      auth: new AuthFake(),
      remoteDb: new RemoteDbFake(),
    }

    const action$ = ActionsObservable.of(
      saveEvent({ id: 'newEventKey', name: 'eventName'})
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = saveEventEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      saveError('Failed.')
    ]);
  });

  test('Send fail action on save Event fail does not affect save Event success', async () => {
    jest.mock('../../../app/remoteDb/RemoteDbFake');
    const mockSaveEvent = jest.fn();
    RemoteDbFake.prototype.saveEvent = mockSaveEvent;
    
    mockSaveEvent
    .mockRejectedValueOnce(new Error('Failed.'))
    .mockImplementationOnce(() => Promise.resolve());

    const dependencies: Dependencies = {
      nav: createHashHistory(),
      auth: new AuthFake(),
      remoteDb: new RemoteDbFake(),
    }

    const action$ = ActionsObservable.of(
      saveEvent({ id: 'newEventKey', name: 'eventName'}),
      saveEvent({ id: 'newEventKey', name: 'eventName'}),
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = saveEventEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      saveError('Failed.'),
      saveSuccess(),
    ]);
  });

  // GET EVENT
  
  test('Send success action on get Event success.', async () => {

    // optional mocking

    const dependencies: Dependencies = {
      nav: createHashHistory(),
      auth: new AuthFake(),
      remoteDb: new RemoteDbFake(),
    }

    const action$ = ActionsObservable.of(
      getEvent('userId', 'getEventId')
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = getEventEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      getSuccess({id: "getEventId", name: "getEventName"})
    ]);
  });

  test('Send fail action on get Event fail.', async () => {
    jest.mock('../../../app/remoteDb/RemoteDbFake');
    const mockGetEvent = jest.fn();
    RemoteDbFake.prototype.getEvent = mockGetEvent;
    
    mockGetEvent
    .mockRejectedValueOnce(new Error('Failed.'));

    const dependencies: Dependencies = {
      nav: createHashHistory(),
      auth: new AuthFake(),
      remoteDb: new RemoteDbFake(),
    }

    const action$ = ActionsObservable.of(
      getEvent('userId', 'getEventId')
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = getEventEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      getError('Failed.')
    ]);
  });

  test('Send fail action on get Event fail does not affect get Event success', async () => {
    jest.mock('../../../app/remoteDb/RemoteDbFake');
    const mockGetEvent = jest.fn();
    RemoteDbFake.prototype.getEvent = mockGetEvent;
    
    mockGetEvent
    .mockRejectedValueOnce(new Error('Failed.'))
    .mockImplementationOnce(() => Promise.resolve({id: "getEventId", name: "getEventName"}));

    const dependencies: Dependencies = {
      nav: createHashHistory(),
      auth: new AuthFake(),
      remoteDb: new RemoteDbFake(),
    }

    const action$ = ActionsObservable.of(
      getEvent('userId', 'getEventId'),
      getEvent('userId', 'getEventId'),
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = getEventEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      getError('Failed.'),
      getSuccess({id: "getEventId", name: "getEventName"}),
    ]);
  });

  // DELETE EVENT

  test('Send success action on delete Event success.', async () => {
    // optional mocking

    const dependencies: Dependencies = {
      nav: createHashHistory(),
      auth: new AuthFake(),
      remoteDb: new RemoteDbFake(),
    }

    const action$ = ActionsObservable.of(
      deleteEvent('userId', 'deleteEventId')
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);
    const epic$ = deleteEventEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      deleteSuccess()
    ]);
  });

  test('Send fail action on delete Event fail.', async () => {
    jest.mock('../../../app/remoteDb/RemoteDbFake');
    const mockDeleteEvent = jest.fn();
    RemoteDbFake.prototype.deleteEvent = mockDeleteEvent;
    
    mockDeleteEvent
    .mockRejectedValueOnce(new Error('Failed.'));

    const dependencies: Dependencies = {
      nav: createHashHistory(),
      auth: new AuthFake(),
      remoteDb: new RemoteDbFake(),
    }

    const action$ = ActionsObservable.of(
      deleteEvent('userId', 'deleteEventId')
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);
    const epic$ = deleteEventEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      deleteError('Failed.')
    ]);
  });

  test('Send fail action on delete Event fail does not affect delete Event success', async () => {
    jest.mock('../../../app/remoteDb/RemoteDbFake');
    const mockDeleteEvent = jest.fn();
    RemoteDbFake.prototype.deleteEvent = mockDeleteEvent;
    
    mockDeleteEvent
    .mockRejectedValueOnce(new Error('Failed.'))
    .mockImplementationOnce(() => Promise.resolve());

    const dependencies: Dependencies = {
      nav: createHashHistory(),
      auth: new AuthFake(),
      remoteDb: new RemoteDbFake(),
    }

    const action$ = ActionsObservable.of(
      deleteEvent('userId', 'deleteEventId'),
      deleteEvent('userId', 'deleteEventId')
    );
    const state$ = new StateObservable<RootState>(new Subject(), state);

    const epic$ = deleteEventEpic(action$, state$, dependencies);

    const result = await epic$.pipe(
      toArray(),
    ).toPromise();

    expect(result).toEqual([
      deleteError('Failed.'),
      deleteSuccess(),
    ]);
  });

});
