import { cleanUp, deleteError, deleteEvent, deleteSuccess, EventState, getError, getEvent, getSuccess, nameChanged, newEvent, saveError, saveEvent, saveSuccess } from "../../../app/features/event/eventActions";
import { eventReducer, initialState } from "../../../app/features/event/eventReducer";

describe('Event Reducer', () => {

  test('initial state', () => {
    const action: any = {};

    expect(eventReducer(undefined, action)).toEqual(initialState)
  });
  
  test('New event', () => {
    const currentState: EventState = {
      loading: true,
      event: { id: "loading", name: "loading" },
      error: undefined,
    };
    const action = newEvent();
    const expectedNextState: EventState = {
      loading: false,
      event: {
        id: 'newEvent',
        name: '',
      },
      error: undefined,
    }

    const actualNextState = eventReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  // name changed
  test('Name changed', () => {
    const currentState: EventState = {
      loading: false,
      event: {
        id: 'newEvent',
        name: ''
      },
      error: undefined,
    };
    const action = nameChanged('Name!');
    const expectedNextState: EventState = {
      loading: false,
      event: {
        id: 'newEvent',
        name: 'Name!',
      },
      error: undefined,
    }

    const actualNextState = eventReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  // save event
  test('Save event', () => {
    const currentState: EventState = {
      loading: false,
      event: {
        id: 'newEvent',
        name: 'Name!'
      },
      error: undefined,
    };
    const action = saveEvent({
      id: 'newEvent',
      name: 'Name!',
    });
    const expectedNextState: EventState = {
      loading: true,
      event: {
        id: 'newEvent',
        name: 'Name!',
      },
      error: undefined,
    }

    const actualNextState = eventReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  // save success
  test('Save success', () => {
    const currentState: EventState = {
      loading: true,
      event: {
        id: 'newEvent',
        name: 'Name!'
      },
      error: undefined,
    };
    const action = saveSuccess();
    const expectedNextState: EventState = {
      loading: false,
      event: {
        id: 'newEvent',
        name: 'Name!',
      },
      error: undefined,
    }

    const actualNextState = eventReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  // save error
  test('Save error', () => {
    const currentState: EventState = {
      loading: true,
      event: {
        id: 'newEvent',
        name: 'Name!'
      },
      error: undefined,
    };
    const action = saveError('Failed.');
    const expectedNextState: EventState = {
      loading: false,
      event: {
        id: 'newEvent',
        name: 'Name!',
      },
      error: 'Failed.',
    }

    const actualNextState = eventReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  // get event
  test('Get event', () => {
    const currentState: EventState = {
      loading: false,
      event: { id: "loading", name: "loading" },
      error: undefined,
    };
    const action = getEvent('userId', 'eventId');
    const expectedNextState: EventState = {
      loading: true,
      event: { id: "loading", name: "loading" },
      error: undefined,
    }

    const actualNextState = eventReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  // get success
  test('Get success', () => {
    const currentState: EventState = {
      loading: true,
      event: { id: "loading", name: "loading" },
      error: undefined,
    };
    const action = getSuccess({ id: 'eventId', name: 'event name', });
    const expectedNextState: EventState = {
      loading: false,
      event: {
        id: 'eventId',
        name: 'event name',
      },
      error: undefined,
    }

    const actualNextState = eventReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  // get error
  test('Get error', () => {
    const currentState: EventState = {
      loading: true,
      event: { id: "loading", name: "loading" },
      error: undefined,
    };
    const action = getError("Failed.");
    const expectedNextState: EventState = {
      loading: false,
      event: { id: "loading", name: "loading" },
      error: "Failed.",
    }

    const actualNextState = eventReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  // clean up
  test('Clean up', () => {
    const currentState: EventState = {
      loading: false,
      event: { id: "abcdef", name: "Name!" },
      error: undefined,
    };
    const action = cleanUp();
    const expectedNextState: EventState = {
      loading: false,
      event: { id: "loading", name: "loading" },
      error: undefined,
    }

    const actualNextState = eventReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  // delete event
  test('Delete event', () => {
    const currentState: EventState = {
      loading: false,
      event: { id: "abcdef", name: "Name!" },
      error: undefined,
    };
    const action = deleteEvent('userId', 'abcdef');
    const expectedNextState: EventState = {
      loading: true,
      event: { id: "abcdef", name: "Name!" },
      error: undefined,
    }

    const actualNextState = eventReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  // delete success
  test('Delete success', () => {
    const currentState: EventState = {
      loading: true,
      event: { id: "abcdef", name: "Name!" },
      error: undefined,
    };
    const action = deleteSuccess();
    const expectedNextState: EventState = {
      loading: false,
      event: { id: "abcdef", name: "Name!" },
      error: undefined,
    }

    const actualNextState = eventReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

  // delete error
  test('Delete error', () => {
    const currentState: EventState = {
      loading: true,
      event: { id: "abcdef", name: "Name!" },
      error: undefined,
    };
    const action = deleteError('Failed.');
    const expectedNextState: EventState = {
      loading: false,
      event: { id: "abcdef", name: "Name!" },
      error: 'Failed.',
    }

    const actualNextState = eventReducer(
      currentState,
      action,
    );
    expect(actualNextState).toEqual(expectedNextState);
  });

});
