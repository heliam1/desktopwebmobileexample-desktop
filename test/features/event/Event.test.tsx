import { CombinedState, configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { cleanup, fireEvent, getByDisplayValue, getByTestId, render, waitForElementToBeRemoved } from "@testing-library/react";
import { AuthFake } from "../../../app/auth/AuthFake";
import { EventState } from "../../../app/features/event/eventActions";
import { RemoteDbFake } from "../../../app/remoteDb/RemoteDbFake";
import { createHashHistory, createMemoryHistory } from "history";
import { ActionsObservable, combineEpics, createEpicMiddleware, StateObservable } from "redux-observable";
import { eventReducer } from "../../../app/features/event/eventReducer";
import { deleteEventEpic, getEventEpic, saveEventEpic } from "../../../app/features/event/eventEpic";
import { catchError } from "rxjs/operators";
import React from "react";
import { Provider } from "react-redux";
import Event from "../../../app/features/event/Event";
import { AuthState } from "../../../app/auth/authActions";
import { authReducer } from "../../../app/auth/authReducer";
import { Route, Router } from "react-router-dom";

afterEach(cleanup)

function setup(
  ui,
  {
    route = '/',
    history = createMemoryHistory({
      initialEntries: [route]
    }),
  } = {},
  preloadedState: {
    auth: AuthState,
    event: EventState
  } = {
    auth: {
      authenticated: true,
      initialized: true,
      userId: 'A valid user id',
    },
    event: {
      loading: true,
      event: {
        id: 'loading',
        name: 'loading',
      },
      error: undefined
    }
  },
) {
  const middleware = [...getDefaultMiddleware()];

  const dependencies = {
    nav: history,
    auth: new AuthFake(),
    remoteDb: new RemoteDbFake(),
  }

  const epicMiddleware = createEpicMiddleware({
    dependencies
  });

  middleware.push(epicMiddleware);

  const store = configureStore({
    reducer: { auth: authReducer, event: eventReducer },
    middleware,
    preloadedState,
  });

  epicMiddleware.run(
    (action$: ActionsObservable<any>, store$: StateObservable<CombinedState<any>>, dependencies: any) => combineEpics(
      saveEventEpic,
      getEventEpic,
      deleteEventEpic,
    )(action$, store$, dependencies).pipe(
      catchError((err, source) => {
        console.error(err);
        return source;
      })
    )
  );

  return {
    ...render(
      <Provider store={store}>
        <Router history={history}>
          {ui}
        </Router>
      </Provider>
    )
  }
}

describe('EventComponent', () => {

  test('initial render', async () => {
    const { queryByTestId, getByText } = setup(
      <Route path='/event/:id'>
        <Event/>
      </Route>,
      {
        route: '/event/newEvent',
      }
    );
    // screen.debug();

    expect(queryByTestId("event_loading"));
    expect(queryByTestId("event_error")).toBeNull();
    expect(queryByTestId("event_delete")).toBeNull();
    expect(queryByTestId("event_id"));
    expect(queryByTestId("event_name"));
    expect(getByText("Save"));
  });

  test('new event', async () => {
    
    const { queryByTestId, getByText } = setup(
      <Route path='/event/:id'>
        <Event/>
      </Route>,
      {
        route: '/event/newEvent',
      }
    );
    
    // If still loading, wait.
    if (queryByTestId("event_loading")) {
      await waitForElementToBeRemoved(() => queryByTestId("event_loading"))
    }

    expect(getByText("newEvent"));
    const nameTextField = queryByTestId("event_name") as HTMLInputElement;
    expect(nameTextField.value).toBe('');
  });

  test('get event', async () => {
    jest.mock('../../../app/remoteDb/RemoteDbFake');
    const mockGetEvent = jest.fn();
    RemoteDbFake.prototype.getEvent = mockGetEvent;
    mockGetEvent
    .mockImplementationOnce(() => Promise.resolve({ id: 'getEventId', name: 'getEventName' }));

    const { queryByTestId, getByText, getByDisplayValue } = setup(
      <Route path='/event/:id'>
        <Event/>
      </Route>,
      {
        route: '/event/getEventId',
      }
    );
    // screen.debug();

    await waitForElementToBeRemoved(() => queryByTestId("event_loading"));
    expect(getByText("getEventId"));

    const nameTextField = getByDisplayValue('getEventName') as HTMLInputElement;
    expect(nameTextField.value).toBe('getEventName');
  });
  
  test('initial name change', async () => {
    const { queryByTestId, getByText } = setup(
      <Route path='/event/:id'>
        <Event/>
      </Route>,
      {
        route: '/event/newEvent',
      }
    );
    // screen.debug();

    if (queryByTestId("event_loading")) {
      await waitForElementToBeRemoved(() => queryByTestId("event_loading"));
    }
    
    const nameInput = queryByTestId("event_name");
    fireEvent.change(nameInput, { target: { value: "name" } });

    const nameTextField = queryByTestId("event_name") as HTMLInputElement;
      expect(nameTextField.value).toBe('name');
  });

  test('Save Event fails then succeeds', async () => {
    jest.mock('../../../app/remoteDb/RemoteDbFake');
    const mockSaveEvent = jest.fn();
    RemoteDbFake.prototype.saveEvent = mockSaveEvent;
    mockSaveEvent
    .mockRejectedValueOnce(new Error('Failed.'))
    .mockImplementationOnce(() => Promise.resolve());

    const mockGetEvent = jest.fn();
    RemoteDbFake.prototype.getEvent = mockGetEvent;
    mockGetEvent
    .mockImplementationOnce(() => Promise.resolve({ id: 'getEventId', name: 'getEventName' }));

    const { findByTestId, queryByTestId, getByText } = setup(
      <Route path='/event/:id'>
        <Event/>
      </Route>,
      {
        route: '/event/getEventId',
      }
    );
    // screen.debug();

    const nameInput = queryByTestId("event_name");
    fireEvent.change(nameInput, { target: { value: "Save me." } });
    fireEvent.click(getByText('Save'));

    expect(queryByTestId("event_loading"));
    expect(queryByTestId('event_error'));

    fireEvent.click(getByText('Save'));
    await waitForElementToBeRemoved(() => queryByTestId("event_loading"));
    expect(queryByTestId("event_error")).toBeNull();

    // Expect that we have navigated away.
    expect(queryByTestId("event_id")).toBeNull();
    expect(queryByTestId("event_name")).toBeNull();
  });

  test('Delete Event fails then succeeds', async () => {
    jest.mock('../../../app/remoteDb/RemoteDbFake');
    
    const mockDeleteEvent = jest.fn();
    RemoteDbFake.prototype.deleteEvent = mockDeleteEvent;
    mockDeleteEvent
    .mockRejectedValueOnce(new Error('Failed.'))
    .mockImplementationOnce(() => new Promise(
      resolve => setTimeout(() => resolve(/*return value*/), 250)
    ));

    const mockGetEvent = jest.fn();
    RemoteDbFake.prototype.getEvent = mockGetEvent;
    mockGetEvent
    .mockImplementationOnce(() => Promise.resolve({ id: 'getEventId', name: 'getEventName' }));

    const { queryByTestId, getByText, getByTestId } = setup(
      <Route path='/event/:id'>
        <Event/>
      </Route>,
      {
        route: '/event/deleteEventId',
      }
    );
    // screen.debug();

    await waitForElementToBeRemoved(() => queryByTestId("event_loading"));

    fireEvent.click(getByTestId("event_delete"));
    await waitForElementToBeRemoved(() => queryByTestId("event_loading"));
    expect(queryByTestId('event_error'));

    fireEvent.click(getByTestId("event_delete"));
    await waitForElementToBeRemoved(() => queryByTestId("event_loading"));
    expect(queryByTestId("event_error")).toBeNull();

    // Expect that we have navigated away.
    expect(queryByTestId("event_id")).toBeNull();
    expect(queryByTestId("event_name")).toBeNull();
  });
});
